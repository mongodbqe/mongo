(env) user@home:~/Downloads/mongo-main/src_attack$ python3 attack.py --uniform 100 --fast
Experiment type = query(uniform)
Experiment number = 0
Assignments refined for field 0: 6.63 seconds
Assignments refined for field 1: 6.68 seconds
Assignments refined for field 2: 6.35 seconds
Assignments refined for field 3: 6.51 seconds
Assignments refined for field 4: 0.00 seconds
Assignments refined for field 5: 6.58 seconds
Progress: 0 / 1000 (0%)
Progress: 100 / 1000 (10%)
Progress: 200 / 1000 (20%)
Progress: 300 / 1000 (30%)
Progress: 400 / 1000 (40%)
Progress: 500 / 1000 (50%)
Progress: 600 / 1000 (60%)
Progress: 700 / 1000 (70%)
Progress: 800 / 1000 (80%)
Progress: 900 / 1000 (90%)
0.010656356811523438 12.244446992874146 12.376014471054077
Time elapsed: 86.94 seconds
Field 0, key: 54/65 (83.08%)
Field 0, doc: 3010991/3132795 (96.11%)
Field 1, key: 39/43 (90.70%)
Field 1, doc: 2476582/3132795 (79.05%)
Field 2, key: 51/77 (66.23%)
Field 2, doc: 933417/3132795 (29.80%)
Field 3, key: 42/47 (89.36%)
Field 3, doc: 2710774/3132795 (86.53%)
Field 4, key: 10/10 (100.00%)
Field 4, doc: 3132795/3132795 (100.00%)
Field 5, key: 72/89 (80.90%)
Field 5, doc: 269041/3132795 (8.59%)

(env) user@home:~/Downloads/mongo-main/src_attack$ python3 attack.py --uniform 300 --fast
Experiment type = query(uniform)
Experiment number = 0
Assignments refined for field 0: 7.04 seconds
Assignments refined for field 1: 6.69 seconds
Assignments refined for field 2: 7.21 seconds
Assignments refined for field 3: 6.58 seconds
Assignments refined for field 4: 0.00 seconds
Assignments refined for field 5: 7.78 seconds
Progress: 0 / 1000 (0%)
Progress: 100 / 1000 (10%)
Progress: 200 / 1000 (20%)
Progress: 300 / 1000 (30%)
Progress: 400 / 1000 (40%)
Progress: 500 / 1000 (50%)
Progress: 600 / 1000 (60%)
Progress: 700 / 1000 (70%)
Progress: 800 / 1000 (80%)
Progress: 900 / 1000 (90%)
0.013364315032958984 15.411556959152222 48.81765818595886
Time elapsed: 126.46 seconds
Field 0, key: 82/95 (86.32%)
Field 0, doc: 3125947/3132795 (99.78%)
Field 1, key: 48/51 (94.12%)
Field 1, doc: 3005447/3132795 (95.94%)
Field 2, key: 109/158 (68.99%)
Field 2, doc: 2076451/3132795 (66.28%)
Field 3, key: 54/60 (90.00%)
Field 3, doc: 3051001/3132795 (97.39%)
Field 4, key: 10/10 (100.00%)
Field 4, doc: 3132795/3132795 (100.00%)
Field 5, key: 184/228 (80.70%)
Field 5, doc: 1980500/3132795 (63.22%)

(env) user@home:~/Downloads/mongo-main/src_attack$ python3 attack.py --uniform 500 --fast
Experiment type = query(uniform)
Experiment number = 0
Assignments refined for field 0: 7.14 seconds
Assignments refined for field 1: 6.79 seconds
Assignments refined for field 2: 7.79 seconds
Assignments refined for field 3: 6.73 seconds
Assignments refined for field 4: 0.00 seconds
Assignments refined for field 5: 8.47 seconds
Progress: 0 / 1000 (0%)
Progress: 100 / 1000 (10%)
Progress: 200 / 1000 (20%)
Progress: 300 / 1000 (30%)
Progress: 400 / 1000 (40%)
Progress: 500 / 1000 (50%)
Progress: 600 / 1000 (60%)
Progress: 700 / 1000 (70%)
Progress: 800 / 1000 (80%)
Progress: 900 / 1000 (90%)
0.01570749282836914 18.21115469932556 81.16290974617004
Time elapsed: 163.29 seconds
Field 0, key: 83/100 (83.00%)
Field 0, doc: 3127447/3132795 (99.83%)
Field 1, key: 50/51 (98.04%)
Field 1, doc: 3074239/3132795 (98.13%)
Field 2, key: 133/197 (67.51%)
Field 2, doc: 2928845/3132795 (93.49%)
Field 3, key: 55/60 (91.67%)
Field 3, doc: 3093712/3132795 (98.75%)
Field 4, key: 10/10 (100.00%)
Field 4, doc: 3132795/3132795 (100.00%)
Field 5, key: 245/305 (80.33%)
Field 5, doc: 1047501/3132795 (33.44%)

(env) user@home:~/Downloads/mongo-main/src_attack$ python3 attack.py --zipf 100 --fast
Experiment type = query(zipf)
Experiment number = 0
Assignments refined for field 0: 5.77 seconds
Assignments refined for field 1: 6.43 seconds
Assignments refined for field 2: 5.78 seconds
Assignments refined for field 3: 6.09 seconds
Assignments refined for field 4: 0.00 seconds
Assignments refined for field 5: 5.99 seconds
Progress: 0 / 1000 (0%)
Progress: 100 / 1000 (10%)
Progress: 200 / 1000 (20%)
Progress: 300 / 1000 (30%)
Progress: 400 / 1000 (40%)
Progress: 500 / 1000 (50%)
Progress: 600 / 1000 (60%)
Progress: 700 / 1000 (70%)
Progress: 800 / 1000 (80%)
Progress: 900 / 1000 (90%)
0.009080886840820312 11.748956441879272 5.040221691131592
Time elapsed: 73.53 seconds
Field 0, key: 31/37 (83.78%)
Field 0, doc: 2593524/3132795 (82.79%)
Field 1, key: 31/33 (93.94%)
Field 1, doc: 1830156/3132795 (58.42%)
Field 2, key: 32/40 (80.00%)
Field 2, doc: 768534/3132795 (24.53%)
Field 3, key: 28/33 (84.85%)
Field 3, doc: 2358685/3132795 (75.29%)
Field 4, key: 10/10 (100.00%)
Field 4, doc: 3132795/3132795 (100.00%)
Field 5, key: 47/61 (77.05%)
Field 5, doc: 1495370/3132795 (47.73%)

(env) user@home:~/Downloads/mongo-main/src_attack$ python3 attack.py --zipf 300 --fast
Experiment type = query(zipf)
Experiment number = 0
Assignments refined for field 0: 6.61 seconds
Assignments refined for field 1: 6.69 seconds
Assignments refined for field 2: 6.56 seconds
Assignments refined for field 3: 6.59 seconds
Assignments refined for field 4: 0.00 seconds
Assignments refined for field 5: 6.95 seconds
Progress: 0 / 1000 (0%)
Progress: 100 / 1000 (10%)
Progress: 200 / 1000 (20%)
Progress: 300 / 1000 (30%)
Progress: 400 / 1000 (40%)
Progress: 500 / 1000 (50%)
Progress: 600 / 1000 (60%)
Progress: 700 / 1000 (70%)
Progress: 800 / 1000 (80%)
Progress: 900 / 1000 (90%)
0.011455297470092773 12.418927431106567 16.5259530544281
Time elapsed: 88.37 seconds
Field 0, key: 58/74 (78.38%)
Field 0, doc: 610897/3132795 (19.50%)
Field 1, key: 44/47 (93.62%)
Field 1, doc: 2656333/3132795 (84.79%)
Field 2, key: 66/105 (62.86%)
Field 2, doc: 675000/3132795 (21.55%)
Field 3, key: 48/57 (84.21%)
Field 3, doc: 2979186/3132795 (95.10%)
Field 4, key: 10/10 (100.00%)
Field 4, doc: 3132795/3132795 (100.00%)
Field 5, key: 97/122 (79.51%)
Field 5, doc: 1645944/3132795 (52.54%)

(env) user@home:~/Downloads/mongo-main/src_attack$ python3 attack.py --zipf 500 --fast
Experiment type = query(zipf)
Experiment number = 0
Assignments refined for field 0: 7.06 seconds
Assignments refined for field 1: 7.04 seconds
Assignments refined for field 2: 7.41 seconds
Assignments refined for field 3: 6.90 seconds
Assignments refined for field 4: 0.00 seconds
Assignments refined for field 5: 7.96 seconds
Progress: 0 / 1000 (0%)
Progress: 100 / 1000 (10%)
Progress: 200 / 1000 (20%)
Progress: 300 / 1000 (30%)
Progress: 400 / 1000 (40%)
Progress: 500 / 1000 (50%)
Progress: 600 / 1000 (60%)
Progress: 700 / 1000 (70%)
Progress: 800 / 1000 (80%)
Progress: 900 / 1000 (90%)
0.013865470886230469 15.179434061050415 30.653549909591675
Time elapsed: 109.64 seconds
Field 0, key: 71/81 (87.65%)
Field 0, doc: 657991/3132795 (21.00%)
Field 1, key: 45/49 (91.84%)
Field 1, doc: 2661097/3132795 (84.94%)
Field 2, key: 83/134 (61.94%)
Field 2, doc: 1892806/3132795 (60.42%)
Field 3, key: 47/57 (82.46%)
Field 3, doc: 2970951/3132795 (94.83%)
Field 4, key: 10/10 (100.00%)
Field 4, doc: 3132795/3132795 (100.00%)
Field 5, key: 143/182 (78.57%)
Field 5, doc: 1899376/3132795 (60.63%)
