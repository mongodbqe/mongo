"""file: src/main.py (runnable)
Automatically collecting the oplog and querylog data
"""

import shutil
import os
import subprocess
import multiprocessing
import timeit
import time
from enum import Enum
from pathlib import Path
from client import Client
from parse_oplog import OpLogParser
from parse_query_log import LogParser
from make_data_key import makeDataKey
from memory_profiler import profile
from parameters import Parameter, PORT_START, PORT_STR, HOST_STR, MONGOD_PATH, MONGOSH_PATH, MONGOEXPORT_PATH, DB_PATH, DEFAULT_LOG_PATH, MongoTask, ParserTask
from pymongo import MongoClient
import argparse
from util import removeFolderContents

CLEAN_LOG_FOLDERS = True # clean log folders for each run, as parser takes input from all files in the oplog/ querylog

def initMongod(par : Parameter):
    """Initialize mongod, this function is called after replicate set 'rs0' is
    initialized.
    
    Args:
      par: Parameter including database port_num, db_path, db_log_path
    """
    mongod = subprocess.Popen([MONGOD_PATH, "--port", str(par.port_num),
                               "--dbpath", par.db_path, "--fork", "--logpath",
                               par.db_log_path, "--replSet", "rs0"],
                              stdout=subprocess.PIPE)
    
    
def initReplicateSet(port_num : int):
    """Initialize the replicate set, this is required by QE.
    
    Args:
      port_num: port number for localhost serving mongod
    """
    
    c = MongoClient('localhost', port_num, directConnection=True)
    print(f"initReplicateSet: port_num={port_num}")
    config = {'_id': 'rs0', 'members': [
        {'_id': 0, 'host': f'localhost:{port_num}'}]}
    try:
        c.admin.command("replSetGetStatus")
    except Exception as e:
        c.admin.command("replSetInitiate", config)
    c.close()

    
def exportOplog(par: Parameter):
    """Export the raw operational log dumped from `oplog.rs`
    The number of records exported in the oplog is reported `output.stderr`,
    grep from output.stderr to obtain the number_ops exported from oplog.
    
    Args:
      par: configuration parameters

    Output files:
      par.oplog_path: oplog
    """    
    
    output = subprocess.run([MONGOEXPORT_PATH, "--host", HOST_STR, "--port",
                             str(par.port_num), "--db", "local", "--collection",
                             "oplog.rs", "--out", par.oplog_path],
                            capture_output=True, text=True)
    print(output.stdout)
    print(output.stderr)
    output_str = output.stderr
    oplog_num_ops = 0
    export_pos = output_str.find("exported")
    export_num_records_str = output_str[export_pos:]
    split_str = export_num_records_str.split(' ')
    oplog_num_ops = int(split_str[-2])
    temp_log_path = par.oplog_path
    print(f"oplog_num_ops={oplog_num_ops} recorded in raw operation log.")
    par.setOplogNumOps(oplog_num_ops)
    os.rename(temp_log_path, par.oplog_path)
    print(f"Finished exporting raw opLog to {par.oplog_path}.")
    
def exportQueryLog(par: Parameter):
    """Export the raw querylog, namely, the default `mongo.log`
    
    Args:
      par: configuration parameters

    Output files:
      par.log_path: querylog
    """
    
    shutil.copyfile(par.db_log_path, par.log_path)
    print(f"Finished exporting raw queryLog to {par.log_path}.")


def exportSnapshot(par: Parameter):
    """Export the snapshot of the encrypted documents
    
    Args:
      par: configuration parameters

    Output files:
      par.snapshot_path: snapshot of the encrypted documents
    """
    
    output = subprocess.run([MONGOEXPORT_PATH, "--host", HOST_STR, "--port",
                             str(par.port_num), "--db",
                             par.default_encrypted_db_name, "--collection",
                             par.default_encrypted_coll_name, "--out",
                             par.snapshot_path], capture_output=True, text=True)
    print(output.stdout)
    print(output.stderr)

    print(f"Finished exporting the snapshot of the encrypted documents to {par.snapshot_path}.")

    
class MongoLogger:
    """class MongoLogger.
    This is the main class provides the basic functionality:
    - initialization of db
    - insert documents using QE
    - log collections using MongoTask
    """
    
    def __init__(self, par = Parameter()):
        """Initialization
        Create directories
        
        Args:
          par: configuration parameters
        """
        
        self.par = par
        dir_name_l = ['log', 'snapshot', 'oplog']
        for dir_name in dir_name_l:
            exp_str = f"exp_{self.par.exp_num}"            
            Path(f"../{dir_name}/{self.par.dataset_name}/{self.par.dataset_year}/{exp_str}").mkdir(parents=True, exist_ok=True)

    def initDB(self, task_type = MongoTask.CollectOplog):
        """Initialize the database

        Args:
          task_type: MongoTask type
        """
        
        if os.path.exists(self.par.db_path):
            shutil.rmtree(self.par.db_path)
                
        if not os.path.exists(self.par.db_path):
            os.mkdir(self.par.db_path)
            os.mkdir(f"{self.par.db_path}/log")

        if os.path.isfile(self.par.log_path):
            os.remove(self.par.log_path)
            
        print("initDB")
        
        self.proc_mongod = multiprocessing.Process(target = initMongod, args=(self.par,))
        self.proc_mongod.daemon = True
        self.proc_mongod.start()
        
        print("init Mongod")        
        
        self.proc_start_replset = multiprocessing.Process(target = initReplicateSet, args=(self.par.port_num,))
        self.proc_start_replset.start()
        self.proc_start_replset.join()
        self.proc_start_replset.close()
        makeDataKey(self.par)

        
    def closeMongod(self):
        """Close mongod service
        Suppress the mongosh's connection error from the 'shutdown'
        """
        
        output = subprocess.run([MONGOSH_PATH, "--eval",
                                 "db.adminCommand({shutdown : 1})"],
                                stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        self.proc_mongod.terminate()
        self.proc_mongod.close()
        

    def processSubMongoTask(self, task_type : MongoTask, par : Parameter):
        """Process MongoTask: collect oplog, collect querylog, collect querylog and oplog together

        Args:
          task_type: MongoTask
          par: configuration parameters
        """

        self.client.setParameter(par)
        # print(par)
        
        if task_type == MongoTask.CollectOplog:
            print(f"Start insertion:")
            insert_time_str = timeit.Timer(self.client.insertDocumentsFromJSON).timeit(number=1)
            print(f"Finished insertion: {insert_time_str:.4} seconds.")

        elif task_type in [MongoTask.CollectQuerylog, MongoTask.CollectQuerylogOplog]:
            print(f"Start insertion:")
            self.client.setLogLevel(1)
            
            insert_time_str = timeit.Timer(self.client.insertDocumentsFromJSON).timeit(number=1)
            print(f"Finished insertion: {insert_time_str:.4} seconds.")
        else:
            pass
        
        
    def processMongoTask(self, task_type : MongoTask):
        """Process batched Mongo tasks
        - Always ensure the file_name follows the default formatting
        
        Args:
          task_type: MongoTask

        Input files:
          If task_type == MongoTask.CollectQuerylog or MongoTask.CollectQuerylogOplog:
          self.par.find_fields_json_path, (secret) find field values
        
        Ouput files:
          If task_type == MongoTask.CollectOplog: oplog
          If task_type == MongoTask.CollectQuerylog: querylog, snapshot
          If task_type == MongoTask.CollectQuerylogOplog: oplog, querylog, snapshot
        """
        self.initDB(task_type)
        self.client = Client(self.par, task_type)
        
        print(f"Start insertion:")
        
        if task_type == MongoTask.CollectUnencryptedStats:
            insert_time_str = timeit.Timer(self.client.insertUnencryptedDocumentsFromJSONDir).timeit(number=1)
            print(f"Finished insertion: {insert_time_str:.4} seconds.")
            return
        else:
            exp_str = f"exp_{self.par.exp_num}"
            if task_type == MongoTask.CollectOplog:
                log_path = Path(f"../oplog/{self.par.dataset_name}/{self.par.dataset_year}/{exp_str}")
                if CLEAN_LOG_FOLDERS:
                    removeFolderContents(log_path)
            elif task_type == MongoTask.CollectQuerylog:
                log_path = Path(f"../log/{self.par.dataset_name}/{self.par.dataset_year}/{exp_str}")
                if CLEAN_LOG_FOLDERS:
                    removeFolderContents(log_path)
            elif task_type == MongoTask.CollectQuerylogOplog:
                if CLEAN_LOG_FOLDERS:
                    removeFolderContents(Path(f"../log/{self.par.dataset_name}/{self.par.dataset_year}/{exp_str}"))
                    removeFolderContents(Path(f"../oplog/{self.par.dataset_name}/{self.par.dataset_year}/{exp_str}"))                
            
            dataset_dir = self.par.dataset_path
            total_num_records = 0
            time_start = time.perf_counter()
            
            for fn in os.listdir(dataset_dir):
                f_path = dataset_dir + '/' + fn
                if f_path.endswith('.json'):
                    print(f_path)
                    f_name_split = f_path.split("/")[-1].split("_")
                    if len(f_name_split) == 2:
                        file_name = ""
                        num_records = f_name_split[0]
                    else:
                        file_name = f_name_split[0]
                        num_records = f_name_split[1]
                    print(num_records)
                    total_num_records += int(num_records)
                    print(f"total_num_records={total_num_records}")
                    
                    par = Parameter(dataset_name=self.par.dataset_name,
                                    dataset_year = self.par.dataset_year,
                                    file_name=file_name,
                                    dataset_num_records=num_records,
                                    use_data_dir=self.par.use_data_dir,
                                    use_one_log=self.par.use_one_log)
                    par.dataset_json_path = f_path
                    if task_type in [MongoTask.CollectOplog, MongoTask.CollectQuerylog, MongoTask.CollectQuerylogOplog]:
                        self.processSubMongoTask(task_type, par)
                        time_stop = time.perf_counter()
                        print(f"Total Insertion time: {time_stop - time_start}")

        # Limit: only one log (querylog) is used.
        if self.par.use_one_log == True:
            print(f"total_num_records={total_num_records}")
            
            self.par.setNumRecords(total_num_records)
            
            if task_type == MongoTask.CollectOplog:
                print(f"Start compaction:")
                compact_time_str = timeit.Timer(self.client.compact).timeit(number=1)
                print(f"Finished compaction: {compact_time_str:.4} seconds.")
                exportOplog(self.par)
                
            elif task_type == MongoTask.CollectQuerylog:
                self.client.checkDocuments()
                print(f"Start find operations:")
                self.client.setParameter(self.par)
                find_time_str = timeit.Timer(self.client.collectFindQueryLogData).timeit(number=1)
                print(f"Finished finds: {find_time_str:.4} seconds.")
                exportQueryLog(self.par)
                exportSnapshot(self.par)

            elif task_type == MongoTask.CollectQuerylogOplog:
                self.client.setParameter(self.par)
                self.client.checkDocuments()
                print(f"Start find operations:")
                find_time_str = timeit.Timer(self.client.collectFindQueryLogData).timeit(number=1)
                print(f"Finished finds: {find_time_str:.4} seconds.")
                exportQueryLog(self.par)
                exportSnapshot(self.par)
                print(f"Start compaction:")
                compact_time_str = timeit.Timer(self.client.compact).timeit(number=1)
                print(f"Finished compaction: {compact_time_str:.4} seconds.")                
                exportOplog(self.par)
            else:
                pass
                
        self.client.closeConnection()
        self.closeMongod()                
        

    def processParserTask(self, task_type : ParserTask):
        """Parse log
        - Now only supports one file located in the oplog and log folder

        Args:
          task_type: ParserTask
        
        Input files:
          If task_type == ParserTask.Oplog: oplog
          If task_type == ParserTaks.Querylog: querylog
        
        Output files:
          doc_matrix: leakage profile extracted from either oplog or querylog
        """
        
        log_dir = ""
        exp_str = f"exp_{self.par.exp_num}"
        
        if task_type == ParserTask.Oplog:
            log_dir = f"../oplog/{self.par.dataset_name}/{self.par.dataset_year}/{exp_str}"
        elif task_type == ParserTask.Querylog or task_type == ParserTask.OpFindQuerylog:
            log_dir = f"../log/{self.par.dataset_name}/{self.par.dataset_year}/{exp_str}"
        else:
            return
        
        print(log_dir)
        
        for fn in os.listdir(log_dir):
            f_path = log_dir + '/' + fn
            if f_path.endswith('.json'):
                print(f_path)
                f_path_split = f_path.split("/")[-1].split("_")
                if task_type == ParserTask.Oplog:
                    num_ops = int(f_path_split[1])
                    num_records = int(f_path_split[2])
                    par = Parameter(dataset_name=self.par.dataset_name,
                                    dataset_year=self.par.dataset_year,
                                    dataset_num_records=num_records,
                                    oplog_num_ops=num_ops,
                                    exp_num=self.par.exp_num,                                    
                                    use_data_dir=True)
                else:
                    num_records = int(f_path_split[3])
                    par = Parameter(dataset_name=self.par.dataset_name,
                                    dataset_year=self.par.dataset_year,
                                    dataset_num_records=num_records,
                                    exp_num=self.par.exp_num,
                                    use_data_dir=True)
                print(f_path)
                if task_type == ParserTask.Oplog:
                    log_parser = OpLogParser(par)
                elif task_type == ParserTask.Querylog:
                    log_parser = LogParser(par)
                elif task_type == ParserTask.OpFindQuerylog:
                    par.use_find_query_with_oplog = True
                    log_parser = LogParser(par)
                parse_op_time_str = timeit.Timer(log_parser.run).timeit(number=1)
                print(f"Finished parsing {f_path} for {parse_op_time_str:.4} seconds.")

                
    def run(self):
        """The following commands will collect oplog leakage and query leakage
        (with/without length leakage) using a single instance of mongodb, first
        insertions, then perform find query, export the query log; finally
        compaction, export the oplog.
        """
        
        self.processMongoTask(MongoTask.CollectQuerylogOplog)
        self.processParserTask(ParserTask.Oplog)
        self.processParserTask(ParserTask.Querylog)
        

if __name__ == "__main__":
    """Limit: please use use_data_dir=True
    """
    
    parser = argparse.ArgumentParser("MongoDB instance")
    parser.add_argument("--instance", dest='instance', metavar='i', type=int,
                        help="specifies the number of DB instances, default value is 0")
    parser.add_argument("--exp", dest='expnum', metavar='e', type=int,
                        help="specifies the exp_num, default value is 0")
    
    args = parser.parse_args()
    if args.instance != None:
        print(f"Instance={args.instance}")
    else:
        args.instance = 0
        
    if args.expnum != None:
        print(f"Experiment number ={args.expnum}")
    else:
        args.expnum = 0
        
    port_num = PORT_START + args.instance * 4
    connection_str = f"{HOST_STR}:{port_num}"
    print(f"Connected {connection_str}.")
    par = Parameter(dataset_name="acs", dataset_year="2013", use_data_dir=True,
                    port_num=port_num, use_one_log=True, exp_num=args.expnum)
    logger = MongoLogger(par)
    logger.run()
