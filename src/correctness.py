"""file: correctness.py
This file provides checkQueryLeakageCorrectness() and checkCompactionLeakageCorrectness()
functions.
"""
from parameters import Parameter, DEFAULT_CONTENTION_FACTOR
import json

def checkQueryLeakageCorrectness(par : Parameter, doc_matrix_path : str):
    """Check the correctness of the leakage extraction (simplified and also use
     attack to demonstrate the correctness).
    
    - Use the frequency information (aux_info) on the tuple values and field
    values of the plaintext (not used in the attack).
    - Compute the tuple and unique field frequencies from the doc_matrix
    - Check whether the tuple frequency and also unique field frequency are the
    same as in the plaintext.
    - Query leakage with length is ignored as the frequency information stays
    the same, and only the length information is added.
    
    Args:
      par: configuration parameters
      doc_matrix_path: leakage profile
    """
    
    # Load plaintext for frequency comparison
    with open(par.aux_info_len_leakage_path, 'r') as infile:
        json_l = []
        for line in infile:
            json_l.append(json.loads(line))
        fields_sorted_len_leakage = json_l[0]

    tuple_freq_map = json_l[1]
    tuple_freq_l = list(tuple_freq_map.values())
    tuple_data_l = list(tuple_freq_map.keys())
    tuple_freq_l.sort(reverse=True)
    
    doc_mat_query_file = open(doc_matrix_path, 'r')
    
    extracted_tuple_freq_map = {}
    
    unique_field_freq_mmap_l = []
    extracted_unique_field_freq_mmap_l = []

    num_field_names = len(par.export_field_name_list)
    field_freq_l = []
    extracted_field_freq_l = [] 

    for idx in range(num_field_names):
        unique_field_freq_mmap_l.append({})
        extracted_unique_field_freq_mmap_l.append({})

    for tuple_data_str, tuple_freq in tuple_freq_map.items():
        tuple_data = tuple_data_str[1: -1]
        tuple_data_l = tuple_data.replace(' ', '').split(',')
        for idx, field_id in enumerate(tuple_data_l):
            if field_id not in unique_field_freq_mmap_l[idx]:
                unique_field_freq_mmap_l[idx][field_id] = 1 * tuple_freq
            else:
                unique_field_freq_mmap_l[idx][field_id] = unique_field_freq_mmap_l[idx][field_id] + 1 * tuple_freq

    for idx in range(num_field_names):
        single_field_freq_l = list(unique_field_freq_mmap_l[idx].values())
        single_field_freq_l.sort(reverse=True)
        field_freq_l.append(single_field_freq_l)

    for line in doc_mat_query_file:
        json_data = json.loads(line)
        tuple_data = []

        for i in range(num_field_names):
            key_i = f"{i}"
            field_id = json_data[key_i][0]
            tuple_data.append(field_id)
            if field_id not in extracted_unique_field_freq_mmap_l[i]:
                extracted_unique_field_freq_mmap_l[i][field_id] = 1
            else:
                extracted_unique_field_freq_mmap_l[i][field_id] = extracted_unique_field_freq_mmap_l[i][field_id] + 1

        key_tuple = f"{tuple_data}"

        if key_tuple not in extracted_tuple_freq_map:
            extracted_tuple_freq_map[key_tuple] = 1
        else:
            extracted_tuple_freq_map[key_tuple] = extracted_tuple_freq_map[key_tuple] + 1

    extracted_tuple_freq_l = list(extracted_tuple_freq_map.values())
    extracted_tuple_freq_l.sort(reverse=True)
    
    # print(len(tuple_freq_l))
    # print(len(extracted_tuple_freq_l))
    # print(extracted_unique_field_freq_mmap_l)

    for idx in range(num_field_names):
        extracted_single_field_freq_l = list(extracted_unique_field_freq_mmap_l[idx].values())
        extracted_single_field_freq_l.sort(reverse=True)
        extracted_field_freq_l.append(extracted_single_field_freq_l)

    # print(extracted_field_freq_l)
    assert(tuple_freq_l == extracted_tuple_freq_l)
    assert(field_freq_l == extracted_field_freq_l)
    doc_mat_query_file.close()

def checkCompactionLeakageCorrectness(par : Parameter, doc_matrix_path : str):
    """Check the correctness of the leakage extraction.
    - Using the number of tokens observed for each field.
    - With a high probablity for a large number of records (> 3 million) matching the exact number: for each field name, the number of
     (all unique field valeus + empty string (if present)) * (max_contention factor + 1)
    - With smaller number of data points, we ensure that it is smaller than the numbers above.

    Args:
      par: configuration parameters
      doc_matrix_path: leakage profile
    """
    unique_fields_empty_sorted_l = []
    
    doc_mat_file = open(doc_matrix_path, 'r')
    
    with open(par.unique_fields_empty_sorted_json_path, 'r') as infile:
        line = infile.read()
        unique_fields_empty_sorted_l = json.loads(line)

    num_field_names = len(par.export_field_name_list)
    unique_fields_len_l = []
    token_cf_set_l = []
    
    for idx, field_name in enumerate(par.export_field_name_list):
        unique_len_fields_empty = len(unique_fields_empty_sorted_l[field_name])
        unique_fields_len_l.append(unique_len_fields_empty)
        token_cf_set_l.append(set([]))

    full_num_unique_token_cf_l = [e * (par.contention_factor + 1) for e in unique_fields_len_l]

    for line in doc_mat_file:
        json_data = json.loads(line)
        
        for i in range(num_field_names):
            key_i = f"{i}"
            field_id = json_data[key_i][0]
            token_cf_set_l[i].add(field_id)
            
    num_unique_token_cf_l = []
    for i in range(num_field_names):
        num_unique_token_cf_l.append(len(token_cf_set_l[i]))
        
    print("Possible number of unique esc cf tokens for each field name:")
    print(full_num_unique_token_cf_l)

    if par.dataset_num_records >= 3000000 and par.contention_factor == DEFAULT_CONTENTION_FACTOR:
        if (full_num_unique_token_cf_l == num_unique_token_cf_l):
            pass
            # print("Pass the correctness check for compaction leakage (oplog).")
        else:
            print("Faild the simple correctness check (should pass with a high probability) for compaction leakage (oplog). This checks whether the number of (observed) tokens matching the maximum possible number. Due to the randomness from contention factor, this correctness check does not necessarily hold, and the correctness of the leakage profile can be further verified using inference attack.")
            
    else:
        for fid, num_unique_token_cf in enumerate(num_unique_token_cf_l):
            assert(num_unique_token_cf <= full_num_unique_token_cf_l[fid])
