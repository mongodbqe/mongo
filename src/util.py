"""file: src/util.py
Provides utility functions
"""
from pymongo import MongoClient
import pprint # for printing data structure
import datetime # used for the test database
import csv # parsing raw *.csv
import uuid
import numpy as np
from pathlib import Path

def fillLowerTriSymmetric(X):
    """Fill the lower triangle symmetric to the upper one

    Args:
      X: matrix

    Returns:
      X: matrix
    """
    X = X + X.T - np.diag(np.diag(X))
    return X


def isValidUUID(val):
    """Test whether given uuid_str is valid

    Args:
      val: str

    Returns:
      bool or None
    """    
    try:
        return uuid.UUID(str(val))
    except ValueError:
        return None

def removeFolderContents(path: Path):
    """Recursively remove the folders or symlinks
    
    Args:
      path: Path

    Returns:
      None
    """
    
    if path.exists():
        if path.is_file() or path.is_symlink():
            path.unlink()
            return        
        for p in path.iterdir():
            removeFolderContents(p)
        
