"""file: src/export_acs_data_simulated.py (runnable)
1. Use 2013 as the source data, generating the leakage profile
3. Export unique field values for each field name.
"""

from parameters import *
import os
from pathlib import Path
from DataExporter import DataExporter
from ACSDataExporter import ACSDataExporter
import time
import argparse
from util import removeFolderContents

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Export ACS Data simulated leakage")
    parser.add_argument("--start", dest='start', metavar='s', type=int,
                        help="specifies the starting exp_num, experiment number")
    parser.add_argument("--end", dest='end', metavar='e', type=int,
                        help="specifies the ending exp_num (exclusive), experiment number")
    parser.add_argument("--limit", dest='limit', metavar='l', type=int,
                        help= ("number of records, please specify an even number,"
                               "default value = -1, will export the whole dataset"))
    args = parser.parse_args()

    assert(args.start != None)
    assert(args.end != None)    
    
    dataset_year = "2013"
    
    if args.limit != None:
        print(f"--limit={args.limit}")
        limit_num_records = int(args.limit / 2) # two in total
        total_num_records = limit_num_records * 2        
    else:
        args.limit = -1
        limit_num_records = -1
        if dataset_year == "2013":
            total_num_records = 3132795 # total size of 2013 data
        else:
            print(f"Only use 2013 data set for leakage simluation.")

    removeFolderContents(Path(f"../dataset/acs/{dataset_year}"))
    removeFolderContents(Path(f"../doc_matrix/compaction/acs/{dataset_year}"))
    removeFolderContents(Path(f"../doc_matrix/query/acs/{dataset_year}"))
    
    par = Parameter("acs", dataset_year="2013", dataset_num_records=total_num_records, use_data_dir=True, use_subset=True)
    exporter = ACSDataExporter(parameter=par)
    exporter.parseCSVCode()
    time_a = time.perf_counter()
    exporter.exportMultipleExperimentData(exp_start=args.start, exp_end=args.end, limit_num_records=limit_num_records)
    time_b = time.perf_counter()
    print(f"Total leakage simulation time:{time_b - time_a:.4} seconds.")
