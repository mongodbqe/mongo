"""file: src/ACSDataExporter.py
Implements DataExporter for ACS dataset.
"""
import pandas as pd
import numpy as np
from parameters import *
import csv
import json
import os
from pathlib import Path
from collections import defaultdict
from DataExporter import DataExporter
import time
import argparse
import gzip

class ACSDataExporter(DataExporter):
    def __init__(self, parameter : Parameter):
        """Initialization
        
        Args:
          parameter: configuration parameters
        """
        DataExporter.__init__(self, parameter)

   
    def parseCSVCode(self):
        """Race: Header is in the 2nd row, use two columns, "PUMS Code" + "Race 3 Description"
        RAC1P: Race 1 code for 2019-2013
        RAC3P05: Race 3 code for 2019-2011
        RAC3P12: Race 3 code for 2012-2013
        --------------------
        Place of work: divided into 2012-2013 and 2009-2011
        --------------------
        Place of birth: divided into 2012-2013 and 2009-2011
        --------------------
        Occupation:
        OCCP02 & SOCP00: 2009
        OCCP10 & SOCP10: 2010-2011
        OCCP12 & SOCP12: 2012-2013
        codemap stores the mapping between the encodings and the length
        """
        
        pd.set_option('display.max_columns', None)
        pd.set_option('display.max_rows', None)
        
        self.codemap = {}
        for col_name in self.par.ACS_CSV_COL_NAMES:
            self.codemap[col_name] = {}
        
        for csv_name in self.par.ACS_CSV_CODE_NAMES:
            csv_code_path = f"{self.par.ACS_CSV_CODELIST_PATH}/{csv_name}.csv"

            with open(csv_code_path) as infile:
                if csv_name == "COW":
                    df = pd.read_csv(infile, dtype=str, sep=';')
                else:
                    df = pd.read_csv(infile, dtype=str, na_filter=False)
            used_col_names = []
            header_row_id = 0
            header = []
            code_name = ""
            
            if csv_name in ["RAC3P05", "RAC3P12"]:
                header_row_id = 2
                code_name = "RAC3P"
                
            elif csv_name == "Place of work":
                header_row_id = 3
                code_name = "POWSP"
                                    
            elif csv_name == "Place of birth":
                header_row_id = 3
                code_name = "POBP"
                
            elif csv_name == "OCCP12 & SOCP12":
                header_row_id = 13
                code_name = "OCCP"
                
            elif csv_name == "ST":
                header_row_id = 0
                code_name = "ST"

            elif csv_name == "COW":
                header_row_id = 0
                code_name = "COW"
                
            else:
                continue
                print(f"{csv_name} is not found.")

            header = df.iloc[header_row_id, :].copy()
            if csv_name in ["Place of work", "Place of birth"]:
                header[0] = "Code"
                header[2] = "Description"

            elif csv_name in ["RAC3P05", "RAC3P12"]:
                header[0] = "Code"
                header[1] = "Description"                
                    
            elif csv_name in ["OCCP12 & SOCP12"]:
                header[0] = "Code"
                header[2] = "Description"
                
            elif csv_name in ["ST", "COW"]:
                header = ["Code", "Description"]
            else:
                print(f"{csv_name} is not found.")

            df.columns = header            
            # print(df['Code'].head(10))
            code_str_len = 0
            
            if csv_name in ["Place of work", "Place of birth", "RAC1P", "RAC3P05", "RAC3P12"]:
                code_str_len = 3
            elif csv_name in ["OCCP12 & SOCP12"]:
                code_str_len = 4
            elif csv_name == "ST":
                code_str_len = 2
            elif csv_name == "COW":
                code_str_len = 1

            for idx in df.index:
                if len(df["Code"][idx]) == code_str_len:
                    temp_code = df["Code"][idx]
                    temp_description = df["Description"][idx]
                    self.codemap[code_name][temp_code] = temp_description


    def exportDataset(self, csv_file_name, csv_file_path, limit_num_records=-1):
        """Export csv to JSON for insertion to MongoDB
        1. Read a CSV
        2. Convert a CSV to json
        
        RAC3P: Racial profile level 3
        POBP: Place of birth
        POWSP: Place of work
        COW: Class of worker
        ST: State code
        OCCP: Occupation code

        Args:
          csv_file_name: file name
          csv_file_path: file path
          limit_num_records=-1: number of records, default no limit
        
        Input files:
          csv_file_path: person records in csv

        Output files
          par.dataset_json_path: person records in JSON
        """
        
        with open(csv_file_path) as infile:
            df = pd.read_csv(infile, dtype=str)
            df_processed = df[self.par.ACS_CSV_COL_NAMES]
            num_records = len(df_processed.index)

        if limit_num_records != -1:
            num_records = limit_num_records
        
        par = Parameter(dataset_name=self.par.dataset_name, dataset_year=self.par.dataset_year, file_name=csv_file_name, dataset_num_records=num_records, exp_num=self.par.exp_num)
        print(f"Read num_records={par.dataset_num_records}")
        print(f"JSON data path={par.dataset_json_path}")
        df_processed = df_processed.fillna('')
        df_processed = df_processed.astype(str)

        json_data_l = df_processed.agg(lambda x: x.to_dict(), axis=1)

        """Uniform random choices
        """
        if limit_num_records != -1:
            print(f"Limit the total number of records to {limit_num_records}")
            json_data_l = np.random.choice(json_data_l, limit_num_records, replace=False)
            
        with open(par.dataset_json_path, 'w') as f:
            for json_data in json_data_l:
                f.write(f"{json.dumps(json_data)}\n")
        print(f"Finished exporting to {par.dataset_json_path}")

    def exportDatasetFromDir(self, limit_num_records=-1):
        """Export all dataset in csv under a year to JSON
        
        Args:
          limit_num_records=-1

        Input files:
          csv_data_path: person records in csv in a single year
        """
        csv_data_path = f"../acs_data/{self.par.dataset_year}_person_records"
        
        for fn in os.listdir(csv_data_path):
            if fn.endswith('.csv'):
                f_path = csv_data_path + '/' + fn
                print(f_path)                
                fn_no_extension = os.path.splitext(fn)[0]
                self.exportDataset(fn_no_extension, f_path, limit_num_records)


    def exportMultipleExperimentData(self, exp_start = 0, exp_end = 1, limit_num_records = -1):
        """First create multiple random datasets, then generate simulated leakage

        Args:
          exp_start = 0, starting number of experiment
          exp_end = 1, ending (exclusive) number of experiment
          limit_num_records = -1, number of records, default no limit
        """
        
        if self.par.use_subset:
            for exp_id in range(exp_start, exp_end):
                self.par.setExpNum(exp_id)
                exporter = ACSDataExporter(self.par)
                exporter.parseCSVCode()
                exporter.exportDatasetFromDir(limit_num_records=limit_num_records)
                exporter.exportUniqueFieldsFromDir()
                exporter.exportAuxInfoFromDir()

        super().exportMultipleExperimentData(exp_start, exp_end, limit_num_records)


    def exportUniqueFields(self, dataset_json_path : str):
        """Export unique fields with/without length information
        - Compute unique_fields_with_empty (by insertion order)
        - Compute unique_fields_with_empty_sorted (by sorted order)
        - Compute unqiue_fields (by insertion order)
        - Compute unique_fields_sorted (by sorted order)

        Args:
          dataset_json_path : str
        
        Input file:
          dataset_json_path
                
        """
        
        with open(dataset_json_path, 'r') as infile:
            df = pd.read_json(dataset_json_path, dtype=str, lines=True)

        for field_name in self.par.export_field_name_list:
            df_with_empty = df[field_name]
            unique_field_empty_l = df_with_empty.unique().tolist()
            temp_unique_empty_field_l = self.json_data_with_empty[field_name] + unique_field_empty_l
            self.json_data_with_empty[field_name] = list(set(temp_unique_empty_field_l))
            self.json_data_with_empty_sorted[field_name] = sorted(self.json_data_with_empty[field_name])
            self.json_data[field_name] = list(filter(None, self.json_data_with_empty[field_name]))
            self.json_data_sorted[field_name] = sorted(self.json_data[field_name])

            
    def exportUniqueFieldsFromDir(self):
        """Export unique fields and sorted unique fields from a directory of json_data files

        Output files:
          unique_fields_json: by insertion order
          unique_fields_sored_json: without empty, by sorted order
          unique_fields_with_empty_sored_json: with empty, by sorted order
        """
        self.json_data = {}
        self.json_data_sorted = {}
        self.json_data_with_empty = {}
        self.json_data_with_empty_sorted = {}
        
        for field_name in self.par.export_field_name_list:
            self.json_data[field_name] = []
            self.json_data_with_empty[field_name] = []
        
        if self.par.use_data_dir == True:
            total_num_records = 0
            for fn in os.listdir(self.par.dataset_path):
                f_path = self.par.dataset_path + '/' + fn
                if f_path.endswith('.json'):
                    print(f_path)
                    num_records = f_path.split("/")[-1].split("_")[1]
                    total_num_records = total_num_records + int(num_records)
                    par = Parameter(dataset_name="acs", dataset_num_records=num_records,
                                    use_data_dir=True)
                    self.exportUniqueFields(f_path)
            self.par.setNumRecords(total_num_records)
        else:
            self.exportUniqueFields(self.par.dataset_json_path)

        for field_name in self.par.export_field_name_list:
            print(f"Field Name={field_name}")
            field_len_max = max(list(map(lambda x: len(x), self.json_data[field_name])))
            print(f"field_len_max={field_len_max}")
            num_unique_fields = len(self.json_data[field_name])
            print(f"num_unique_fields (without empty) ={num_unique_fields}")
            num_unique_fields_with_empty = len(self.json_data_with_empty[field_name])
            print(f"num_unique_fields (with empty) ={num_unique_fields_with_empty}")
            # print(self.json_data[field_name])
            print("----------------------------------------")

        with open(self.par.unique_fields_json_path, 'w') as outfile:
            outfile.write(json.dumps(self.json_data))
            print(f"Finished exporting unique_fields to {self.par.unique_fields_json_path}")
                    
        with open(self.par.unique_fields_sorted_json_path, 'w') as outfile:
            outfile.write(json.dumps(self.json_data_sorted))
            print(f"Finished exporting unique_fields_sorted to {self.par.unique_fields_sorted_json_path}")
        with open(self.par.unique_fields_empty_sorted_json_path, 'w') as outfile:
            outfile.write(json.dumps(self.json_data_with_empty_sorted))
            print(f"Finished exporting unique_fields_empty_sorted to {self.par.unique_fields_empty_sorted_json_path}")


    def genSortedUniqueFieldLenMap(self):
        """Ordered alphabetically in the unique fields
        1. one with added length leakage (by converting the encoding to the actual description)
        2. one without added length leakage
        """
        
        self.sorted_field_len_map = {}
        self.sorted_field_len_added_leakage_map = {}
        
        with open(self.par.unique_fields_sorted_json_path, 'r') as infile:
            fields_data = json.load(infile)

        length_leakage_mmap = defaultdict(set)
        
        for fn_id, field_name in enumerate(self.par.export_field_name_list):
            sorted_field_len_map = {}
            sorted_field_len_added_leakage_map = {}            
            sorted_field_len_map["-1"] = QE_ENC_FIELD_LEN_OFFSET
            sorted_field_len_added_leakage_map["-1"] = QE_ENC_FIELD_LEN_OFFSET
            sorted_field_l = fields_data[field_name]

            length_leakage_mmap[field_name] = set()
            
            for idx, field in enumerate(sorted_field_l):
                sorted_field_len_map[idx] = len(field) + QE_ENC_FIELD_LEN_OFFSET
                if field != "":
                    temp_added_len_leakage = len(self.codemap[field_name][field]) + QE_ENC_FIELD_LEN_OFFSET
                else:
                    temp_added_len_leakage = QE_ENC_FIELD_LEN_OFFSET
                    
                sorted_field_len_added_leakage_map[idx] = temp_added_len_leakage
                
                self.sorted_field_len_map[fn_id] = sorted_field_len_map
                self.sorted_field_len_added_leakage_map[fn_id] = sorted_field_len_added_leakage_map
                
                length_leakage_mmap[field_name].add(temp_added_len_leakage)

            print(f"{field_name}: number_of_unique_lengths (with empty) = {len(length_leakage_mmap[field_name])}")
                
    def exportAuxInfo(self, dataset_json_path : str):
        """Create an aux_info_map which stores frequency information of each
        tuple. Each tuple contains number_encrypted_fields identifiers. Each
        identifier uniquely maps to a unique field value under the same field
        name. The identifier used indicates the order of the (sorted) field
        values (generated earlier) under some field name. Using identifiers from
        this ordering prevents introducing other leakage from the original
        dataset, e.g., the insertion order.
        
        Input files:
          dataset_json_path: dataset

        """        
        with open(dataset_json_path, 'r') as infile:
            df = pd.read_json(dataset_json_path, dtype=str, lines=True)
            df = df.fillna('')
            for index, row in df.iterrows():
                f_id_l = []
                for fn_id, field_name in enumerate(self.par.export_field_name_list):
                    if row[field_name] == '':
                        f_id = -1
                    else:
                        f_id = self.fields_sorted_data[field_name].index(row[field_name])
                    f_id_l.append(f_id)
                    f_id_l_key = f"{f_id_l}"
                if f_id_l_key not in self.aux_info_map:
                    self.aux_info_map[f_id_l_key] = 1
                else:
                    self.aux_info_map[f_id_l_key] = self.aux_info_map[f_id_l_key] + 1
   
    def exportAuxInfoFromDir(self):
        """Export auxiliary info from directory
        
        Input files:
          self.par.unique_fields_sorted_json_path, (sorted) unique field values per field name
        
        Output files:
          self.par.aux_info_path, without length leakage
          self.par.aux_info_len_leakage_path, with added length leakage
        """
        self.aux_info_map = {}
        with open(self.par.unique_fields_sorted_json_path, 'r') as infile:
            self.fields_sorted_data = json.load(infile)

        if self.par.use_data_dir == True:
            total_num_records = 0
            for fn in os.listdir(self.par.dataset_path):
                f_path = self.par.dataset_path + '/' + fn
                if f_path.endswith('.json'):
                    num_records = f_path.split("/")[-1].split("_")[1]
                    total_num_records = total_num_records + int(num_records)
                    self.exportAuxInfo(f_path)
            self.par.setNumRecords(total_num_records)
        else:
            self.exportAuxInfo(self.par.dataset_json_path)

        self.genSortedUniqueFieldLenMap()

        with open(self.par.aux_info_path, 'w') as outfile:
            outfile.write(f"{json.dumps(self.sorted_field_len_map)}\n")
            outfile.write(f"{json.dumps(self.aux_info_map)}\n")

        print(f"Finished exporting aux_info to {self.par.aux_info_path}")

        with open(self.par.aux_info_len_leakage_path, 'w') as outfile:
            outfile.write(f"{json.dumps(self.sorted_field_len_added_leakage_map)}\n")
            outfile.write(f"{json.dumps(self.aux_info_map)}\n")

        print(f"Finished exporting aux_info_len_leakage to {self.par.aux_info_len_leakage_path}")
