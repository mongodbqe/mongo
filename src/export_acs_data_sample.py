"""file: src/export_acs_data_sample.py (runnable)
1. Export a randomized subset of specified number of records in JSON format
2. Export the auxiliary information of the dataset, contains the frequency
information, counts of unique tuples
3. Export unique field values for each field name
4. Generate randomized find queries for later queries and kept in the query
answer file to compute the recovery rate.
"""
from parameters import *
import os
import argparse

from pathlib import Path
from ACSDataExporter import ACSDataExporter
from util import removeFolderContents

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Export ACS Data")
    parser.add_argument("--limit", dest='limit', metavar='l', type=int,
                        help= ("number of records, please specify an even number,"
                               "default value = -1, and will export the 3K records"))
    parser.add_argument("--exp", dest='expnum', metavar='e', type=int,
                        help="specifies the exp_num, namely experiment number, default value is 0")
    
    args = parser.parse_args()
    
    dataset_year = "2013"
    
    if args.limit != None:
        print(f"--limit={args.limit}")
        limit_num_records = int(args.limit / 2)
        total_num_records = limit_num_records * 2        
    else:
        args.limit = -1
        limit_num_records = 1500
        total_num_records = limit_num_records * 2

    if args.expnum != None:
        print(f"Experiment number ={args.expnum}")
    else:
        args.expnum = 0

    removeFolderContents(Path(f"../dataset/acs/{dataset_year}/exp_{args.expnum}"))
    par = Parameter("acs", dataset_year="2013", exp_num=args.expnum,
                    dataset_num_records=total_num_records, use_data_dir=True, use_subset=True)
    exporter = ACSDataExporter(parameter=par)
    exporter.parseCSVCode()
    exporter.exportDatasetFromDir(limit_num_records=limit_num_records)
    exporter.exportUniqueFieldsFromDir()
    exporter.exportAuxInfoFromDir()
    exporter.exportQueryAnswer(use_simulated_leakage=False)
