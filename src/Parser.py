"""src/Parser.py
Provide class Parser that supports getLengthLeakage
"""
import os
import json
from collections import defaultdict, OrderedDict

class Parser:
    def __init__(self):
        pass

    def getLengthLeakage(self, num_records):
        """Generate artificial length leakage using codebook
        """
        self.doc_id_field_length_leakage_mmap = defaultdict(list)
        
        fields_sorted_data = {}
        fields_sorted_len_leakage = {}

        with open(self.par.unique_fields_sorted_json_path, 'r') as infile:
            print(f"Load {self.par.unique_fields_sorted_json_path}")
            fields_sorted_data = json.load(infile)

        if self.par.dataset_name == "acs":
            with open(self.par.aux_info_len_leakage_path, 'r') as infile:
                print(f"Load {self.par.aux_info_len_leakage_path}")
                json_l = []
                for line in infile:
                    json_l.append(json.loads(line))
                fields_sorted_len_leakage = json_l[0]

        doc_id = 0
        sorted_insertion_id_mmap_l = []
        
        for i in range(self.par.num_encrypted_fields):
            sorted_insertion_id_mmap_l.append(defaultdict(set))
            
        json_l = []

        
        if self.par.use_data_dir == False:
            with open(self.par.dataset_json_path, 'r') as infile:
                json_l = json.load(infile)
        else:
            data_dir = self.par.dataset_path
            for fn in os.listdir(data_dir):
                f_path = data_dir + '/' + fn
                if f_path.endswith('.json'):
                    with open(f_path, 'r') as infile:
                        for line in infile:
                            json_l.append(json.loads(line))

        for doc_id, json_data in enumerate(json_l):
            for fn_idx, fn in enumerate(self.par.export_field_name_list):
                inserted_field = json_data[fn]
                sorted_field_id = -1
                if inserted_field != '':
                    sorted_field_id = fields_sorted_data[fn].index(inserted_field)

                if self.par.dataset_name == "acs":
                    inserted_field_length_leakage = fields_sorted_len_leakage[f"{fn_idx}"][f"{sorted_field_id}"]
                    self.doc_id_field_length_leakage_mmap[doc_id].append(inserted_field_length_leakage)

            doc_id = doc_id + 1
            
        assert(len(json_l) == num_records)
   
