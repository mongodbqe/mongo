"""file: check_config.py
This file is provided to check the completion of library and dataset.
"""
from parameters import CRYPT_SHARED_LIB_PATH_STR, MONGOD_PATH, MONGOSH_PATH, MONGOEXPORT_PATH
from pathlib import Path

def file_exists(f_path : str):
    """Check whether the file exist

    Args:
      f_path: file path
    
    Returns:
      None or exit(1)
    """
    
    my_file = Path(f_path)
    if my_file.is_file():
        print(f"{f_path} is found.")
    else:
        print(f"{f_path} is not found.")
        exit(1)

if __name__ == "__main__":
    """This runnable checks the completeness of configurations of libraries and
    datasets.
    """
    print("Start checking configuration:")
    f_path_l = [CRYPT_SHARED_LIB_PATH_STR,
                MONGOD_PATH,
                MONGOSH_PATH,
                MONGOEXPORT_PATH]
    
    data_year_l = ["12", "13"]
    
    for data_year in data_year_l:
        data_dir_path = f"../acs_data/20{data_year}_person_records"
        fn_l = [f"ss{data_year}pusa.csv", f"ss{data_year}pusb.csv"]
        for fn in fn_l:
            file_exists(data_dir_path + "/" + fn)
        
    for f_path in f_path_l:
        file_exists(f_path)

    print(f"Libraries and datasets are configured successfully.")
    
        

