#!/bin/sh
# file: run_batched_commands.sh
# This file demonstrates how to generate multiple simulated leakage profiles on a multi-core machine

physical_core_start=28
physical_core_end=52

offset=56

batch_id=0
batch_size=4

for ((core_id=physical_core_start;core_id<=physical_core_end;core_id++)); do
    logical_core_id=$(($core_id+56))
    session_name=mongo_${core_id}
    exp_id_start=$(($batch_id*batch_size))
    exp_id_end=$(($exp_id_start+batch_size))

    tmux new-session -d -s $session_name \; send-keys "taskset -c $core_id,$logical_core_id python export_acs_data_simulated.py --start=${exp_id_start} --end=${exp_id_end} --limit=30000" Enter
    batch_id=$(($batch_id+1))
done
