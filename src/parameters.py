from enum import Enum
import json
import pprint

"""Supported tasks
Use CollectQuerylogOplog by defaut
"""
class MongoTask(Enum):
    CollectOplog = 1
    CollectQuerylog = 2
    CollectQuerylogOplog = 3
    CollectUnencryptedStats = 5
    
class ParserTask(Enum):
    Oplog = 1
    Querylog = 2
    OpFindQuerylog = 3

"""MongoDB supported Queryable Encryption operations
"""
class OpType(Enum):
    Insert = 1
    Find = 2
    FindAndDelete = 3

"""Parameters for MongoDB connection
"""
PORT_STR = "27017"
PORT_START = 27017
HOST_STR = "127.0.0.1"
CONNECTION_STRING = f"{HOST_STR}:{PORT_STR}"

KEY_VAULT_DB = "encryption"
KEY_VAULT_COLL = "__keyVault"
KEY_VAULT_NAMESPACE = f"{KEY_VAULT_DB}.{KEY_VAULT_COLL}"

DEFAULT_KMS_PROVIDER = "local"
MASTER_KEY_PATH = "./master-key.txt"

"""TODO: Need to configure the following paths
Please use local directory for DB_PATH.
May want to use `dpkg -L packagename` to find location if the packages are installed using .deb
e.g., `dpkg -L mongodb-mongosh`

Example:
CRYPT_SHARED_LIB_PATH_STR = "/home/ubuntu/Downloads/mongo_crypt_shared/lib/mongo_crypt_v1.so"
MONGOD_PATH = "/home/ubuntu/Downloads/mongodb-enterprise/bin/mongod"
MONGOSH_PATH = "/usr/bin/mongosh"
MONGOEXPORT_PATH = "/usr/bin/mongoexport"
DB_PATH = "/home/ubuntu/db"

"""
CRYPT_SHARED_LIB_PATH_STR = "<path/to/mongo_crypt_v1.so>"
MONGOD_PATH = "<path/to/mongod>"
MONGOSH_PATH = "<path/to/mongosh>"
MONGOEXPORT_PATH = "<path/to/mongoexport>"
DB_PATH = "<path/to/db>"

DEFAULT_LOG_PATH = f"{DB_PATH}/mongo.log"
DEFAULT_LOG_LEVEL = 1
USE_SAFE_CONTENT = False
USE_COMPRESSION = True


"""QE default parameters, AES-CTR-HMAC-SHA256, in number of bytes, offset between ciphertext and plaintext
   215 bytes for encrypting an empty string
"""
QE_ENC_FIELD_LEN_OFFSET = 215
DEFAULT_CONTENTION_FACTOR = 4

class Parameter:
    def __init__(self, dataset_name = "acs", dataset_year = "2012", file_name =
                 "", dataset_num_records : int = 0, contention_factor : int =
                 DEFAULT_CONTENTION_FACTOR, oplog_num_ops : int = 0,
                 use_data_dir = False, use_one_log = True,
                 use_find_query_with_oplog = False,
                 port_num = PORT_START,
                 exp_num = 0,
                 use_subset = False):
        self.dataset_name = dataset_name
        self.dataset_num_records = dataset_num_records
        self.default_log_level = int(DEFAULT_LOG_LEVEL)
        self.file_name = file_name
        self.dataset_year = dataset_year
        self.contention_factor = contention_factor
        self.oplog_num_ops = oplog_num_ops
        self.use_data_dir = use_data_dir
        self.use_one_log = use_one_log
        self.use_find_query_with_oplog = use_find_query_with_oplog
        self.port_num = port_num
        self.connection_str = f"{HOST_STR}:{self.port_num}"
        self.db_path = f"{DB_PATH}_{port_num}"
        self.db_log_path = f"{DB_PATH}_{port_num}/mongo.log"
        self.exp_num = exp_num
        self.use_subset = use_subset
        self.oplog_correctness_vector = [] # number of esc_cf_tokens

        """acs: parameters for ACS Dataset
        wa: parameters for washingtonPMPRecords
        RAC3P05: Race 3 code for 2019-2011
        RAC3P12: Race 3 code for 2012-2013
        OCCP12
        """
        if self.dataset_name == "acs":
            self.default_encrypted_db_name = "acspum"
            self.default_encrypted_coll_name = self.dataset_year
            self.ACS_CSV_CODELIST_PATH = "../acs_data/ACSPUMS2009_2013CodeLists"
            self.ACS_CSV_CODE_NAMES = ["RAC3P12", "ST", "Place of birth", "Place of work", "COW", "OCCP12 & SOCP12"]
            self.ACS_CSV_COL_NAMES = ["RAC3P", "ST", "POBP", "POWSP", "COW", "OCCP"]            
            self.export_field_name_list = self.ACS_CSV_COL_NAMES
            self.export_field_type_list = ["string"] * len(self.export_field_name_list)
            self.find_field_name_list = self.export_field_name_list
            
        else:
            raise ValueError("Dataset name is not supported.")

        self.num_encrypted_fields = len(self.export_field_name_list) # indexed encrypted fields
        self.updatePath()

    def updatePath(self):
        exp_str = f"exp_{self.exp_num}"
        prefix_str = f"{self.dataset_name}/{self.dataset_year}/{exp_str}"
        file_extension_str = f"{self.dataset_num_records}_{self.num_encrypted_fields}.json"

        if self.file_name != "":
            file_extension_str = f"{self.file_name}_" + file_extension_str
        self.dataset_path = f"../dataset/{prefix_str}"
        self.dataset_json_path = f"../dataset/{prefix_str}/{file_extension_str}"
        self.log_path = f"../log/{prefix_str}/level_{self.default_log_level}_command_{file_extension_str}"
        self.oplog_path = f"../oplog/{prefix_str}/oplog_{self.oplog_num_ops}_{file_extension_str}"
        self.snapshot_path = f"../snapshot/{prefix_str}/snapshot_{file_extension_str}"
        self.unique_fields_json_path = f"../fields/{prefix_str}/fields_{self.dataset_num_records}_{self.num_encrypted_fields}.json"
        self.unique_fields_sorted_json_path = f"../fields/{prefix_str}/" \
            f"fields_sorted_{self.dataset_num_records}_{self.num_encrypted_fields}.json"
        self.unique_fields_empty_sorted_json_path = f"../fields/{prefix_str}/" \
            f"fields_with_empty_sorted_{self.dataset_num_records}_{self.num_encrypted_fields}.json"
        self.find_fields_json_path = f"../fields/{prefix_str}_find_fields_{file_extension_str}"
        self.doc_matrix_query_path = f"../doc_matrix/query/{prefix_str}_{file_extension_str}"
        self.doc_matrix_query_with_len_path = f"../doc_matrix/query/{prefix_str}" \
            f"_{self.dataset_num_records}_with_len_{self.num_encrypted_fields}.json"
        self.doc_matrix_compaction_path = f"../doc_matrix/compaction/{prefix_str}_{file_extension_str}"
        self.doc_matrix_compaction_with_len_path = f"../doc_matrix/compaction/{prefix_str}" \
            f"_{self.dataset_num_records}_with_len_{self.num_encrypted_fields}.json"
        self.answer_query_path = f"../answer/query/{prefix_str}_answer_{file_extension_str}"
        self.answer_query_with_len_path = f"../answer/query/{prefix_str}_answer_with_len_{file_extension_str}"
        self.answer_compaction_path = f"../answer/compaction/{prefix_str}_answer_{file_extension_str}"
        self.answer_compaction_with_len_path = f"../answer/compaction/{prefix_str}_answer_with_len_{file_extension_str}"
        
        self.aux_info_path = f"../aux_info/{prefix_str}/aux_info_{file_extension_str}"
        self.aux_info_len_leakage_path = f"../aux_info/{prefix_str}/aux_info_len_leakage_{file_extension_str}"
        self.info_path = f"../info/{prefix_str}/info_{file_extension_str}"

    def setNumRecords(self, num_records : int):
        self.dataset_num_records = num_records
        self.updatePath()

    def setExpNum(self, exp_num : int):
        self.exp_num = exp_num
        self.updatePath()        

    def setOplogNumOps(self, num_ops : int):
        self.oplog_num_ops = num_ops
        self.updatePath()

    def setOplogPath(self, oplog_path : str):
        self.oplog_path = oplog_path

    def setFileName(self, file_name : str):
        """Each dataset is imported by using multiple csv files
        """
        self.file_name = file_name
        self.updatePath()

    def setDatasetYear(self, dataset_year : str):
        self.dataset_year = dataset_year
        self.updatePath()

def createEncryptedFieldsInitMap(par = Parameter()) -> dict:
    """Initialize an encryptedFieldsMap used by QE.
    
    Args:
      par: configuration parameters

    Returns:
      dict that represents the encryptedFieldsMap skeleton
    """
    encrypted_db_coll_name = f"{par.default_encrypted_db_name}.{par.default_encrypted_coll_name}"
    
    encrypted_fields_map = {
        encrypted_db_coll_name: {
            "fields": []            
        }
    }
    
    for i in range(len(par.export_field_name_list)):
        field_path = par.export_field_name_list[i]
        field_type = par.export_field_type_list[i]

        single_field_map = {
            "keyId": 0,
            "path": field_path,
            "bsonType": field_type,
            "queries": {"queryType": "equality"},
        }
        encrypted_fields_map[encrypted_db_coll_name]["fields"].append(single_field_map)
        
    return encrypted_fields_map
