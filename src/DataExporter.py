"""file: src/DataExporter.py
Include a class DataExporter used to implement data_exporter for different datasets.
class ACSDataExporter inherits class DataExporter.
For convenince, this file also provides generate simulated leakage function `genSimulatedLeakage`
"""
from parameters import *
from pathlib import Path
from operator import itemgetter ## get the mapping
from numpy.random import default_rng
import numpy as np
import os
import gzip
import shutil
from correctness import checkCompactionLeakageCorrectness, checkQueryLeakageCorrectness

# Whether check correctness
IF_CHECK_CORRECTNESS = True

class LeakageType(Enum):
    """Class LeakageType
    Supported tasks: Query, QueryWithLen, Compaction, CompcationWithLen
    """
    Query = 1
    QueryWithLen = 2
    Compaction = 3
    CompactionWithLen = 4

class DataExporter:
    """class DataExporter
    This class is used to implement data_exporter for different datasets.
    class ACSDataExporter inherits class DataExporter.
    """
    def __init__(self, parameter : Parameter):
        self.par = parameter
        self.createDirs()

    def createDirs(self):
        """Create directories.
        Our code only supports string-type fields for QE operations.
        """
        dir_name_l = ['aux_info', 'fields', 'dataset', 'answer/compaction', 'answer/query', 'doc_matrix/query', 'doc_matrix/compaction']
        for dir_name in dir_name_l:
            exp_str = f"exp_{self.par.exp_num}"
            if dir_name in ['aux_info', 'fields', 'dataset']:
                Path(f"../{dir_name}/{self.par.dataset_name}/{self.par.dataset_year}/{exp_str}").mkdir(parents=True, exist_ok=True)
            else:
                Path(f"../{dir_name}/{self.par.dataset_name}/{self.par.dataset_year}/").mkdir(parents=True, exist_ok=True)
        self.dtype_option = {}
        for field_name in self.par.export_field_name_list:
            self.dtype_option[field_name] = "string"

    def exportDataset(self):
        raise NotImplementedError()

    def exportUniqueFields(self):
        """export unique field values
        """
        raise NotImplementedError()


    def exportMultipleExperimentData(self, exp_start = 0, exp_end = 1, limit_num_records = -1):
        """1. Generate random permutation of the unique_fields, then export it
        as answer map. Based on the answer map, generate and export the leakage
        profile (doc_matrix file).

        2. If the leakage type is query leakage, then the find queries (answers)
        are also exported used for measure recovery rate
        
        3. Check the correctness of the simulated leakage profiles
        
        Args:
          exp_start = 0, starting at exp_start
          exp_end = 1, ending before exp_end, excluding exp_end
          limit_num_records = -1, default -1, meaning the whole dataset is exported, otherwise, a
          random subset of limit_num_records is exported.

        """
        
        if self.par.dataset_name == "acs":
            leakage_type_l = [LeakageType.Query, LeakageType.QueryWithLen,
                              LeakageType.Compaction,
                              LeakageType.CompactionWithLen]
        elif self.par.dataset_name == "wa":
            leakage_type_l = [LeakageType.Query, LeakageType.Compaction]
        else:
            print(f"Dataset {self.par.dataset_name} is not supported.")
            return
        
        for leakage_type in leakage_type_l:
            for exp_id in range(exp_start, exp_end):
                self.par.setExpNum(exp_id)
                if leakage_type in [LeakageType.Query, LeakageType.QueryWithLen]:
                    self.exportQueryAnswer(use_simulated_leakage=True,
                                           leakage_type=leakage_type,
                                           experiment_num=exp_id)
                else:
                    self.exportSimulatedCompactionAnswer(leakage_type,
                                                         experiment_num=exp_id)
                self.exportSimulatedLeakageFromDir(leakage_type)
                
                if IF_CHECK_CORRECTNESS:
                    self.checkCorrectness(leakage_type)
    

    def genSimulatedLeakage(self, leakage_type : LeakageType):
        """Generate simulated query or compaction leakage with and w/o length
        information using the secret random answer_map, output from
        exportSimulatedCompactionAnswer or exportQueryAnswer(use_simulated_leakage=True).
        
        The length leakage information is for demonstration purpose only, and
        the original data has fixed encoding length, and thus we use the
        codebook to convert the encoding to the underlying text to demonstrate.
        As QE utilizes AES-CTR$ without padding, the offset is fixed at
        QE_ENC_FIELD_LEN_OFFSET bytes.

        Args:
          leakage_type : LeakageType

        Output files:
          doc_matrix file: the leakage profile, each line corresponds to 

        """
        
        print(f"doc_matrix_path={self.doc_matrix_path}")
        
        """With extra length leakage
        """
        if self.par.dataset_name == "acs":
            # print(f"Load {self.par.aux_info_len_leakage_path}")
            with open(self.par.aux_info_len_leakage_path, 'r') as infile:
                json_l = []
                for line in infile:
                    json_l.append(json.loads(line))
                fields_sorted_len_leakage = json_l[0]

        # Default insertion order, unique-ness
        # print(f"Load {self.par.unique_fields_sorted_json_path}")
        with open(self.par.unique_fields_sorted_json_path, 'r') as infile:
            self.sorted_fields_data = json.load(infile)

        # print(f"dataset_num_records={self.par.dataset_num_records}!!!!!")
        doc_matrix_outfile = open(self.doc_matrix_path, 'w')

        # print(f"Load {self.par.dataset_path}")

        for fn in os.listdir(self.par.dataset_path):
            f_path = self.par.dataset_path + '/' + fn
            if f_path.endswith('.json'):
                with open(f_path, 'r') as infile:
                    for line in infile:
                        doc = json.loads(line)
                        doc_field_json = {}
                        doc_field_json['i'] = self.doc_id
                        self.doc_id = self.doc_id + 1

                        for fn_id, field_name in enumerate(self.par.export_field_name_list):
                            unique_field_id = -1

                            if field_name in doc:
                                field_data = doc[field_name]

                                if leakage_type in [LeakageType.Query, LeakageType.QueryWithLen]:
                                    if field_data != "":
                                        unique_field_id = self.find_fields_data[field_name].index(field_data)
                                else:
                                    if field_data == "":
                                        sorted_field_id = -1
                                    else:
                                        sorted_field_id = self.sorted_fields_data[field_name].index(field_data)

                                    cf_fid_l = self.answer_field_l[fn_id][sorted_field_id + 1]
                                    rng = np.random.default_rng()
                                    rand_idx = rng.integers(0, self.par.contention_factor + 1, size=1)[0]
                                    unique_field_id = cf_fid_l[1][rand_idx]

                                if leakage_type in [LeakageType.QueryWithLen,
                                                    LeakageType.CompactionWithLen]:
                                    if field_data == "":
                                        sorted_field_id = -1
                                    else:
                                        sorted_field_id = self.sorted_fields_data[field_name].index(field_data)                            
                                    len_leakage = fields_sorted_len_leakage[f"{fn_id}"][f"{sorted_field_id}"]

                                    efield_len = len_leakage
                                else:
                                    # compute the length leakage
                                    efield_len = QE_ENC_FIELD_LEN_OFFSET + len(field_data)

                                doc_field_json[fn_id] = [unique_field_id, efield_len]
                        doc_matrix_outfile.write(f"{json.dumps(doc_field_json)}\n")
        doc_matrix_outfile.close()

        # default USE_COMPRESSION is on to save space for exporting the doc_matrix
        if USE_COMPRESSION:
            with open(self.doc_matrix_path) as f_in:
                with gzip.open(f"{self.doc_matrix_path}.gz", 'wt') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove(self.doc_matrix_path)
            
    def exportJSONDataListToFile(self, file_name, json_data_l):
        """Export a list of JSON objects to a file
        
        Args:
          file_name: output file path
          json_data_l: a list of JSON objects
        
        File output:
          json_data_l
        """
        with open(file_name, 'w') as outfile:
            for json_data in json_data_l:
                outfile.write(f"{json.dumps(json_data)}\n")
        print(f"Finished exporting processed JSON data to {file_name}.")
        

    def exportSimulatedLeakageFromDir(self, leakage_type : LeakageType):
        """Export simulated leakage, with or w/o length information
        
        Args:
          leakage_type: specifies the leakage type

        """
        
        self.doc_id = 0

        if leakage_type == LeakageType.Query:
            self.doc_matrix_path = self.par.doc_matrix_query_path
        elif leakage_type == LeakageType.QueryWithLen:
            self.doc_matrix_path = self.par.doc_matrix_query_with_len_path
        elif leakage_type == LeakageType.Compaction:
            self.doc_matrix_path = self.par.doc_matrix_compaction_path
        elif leakage_type == LeakageType.CompactionWithLen:
            self.doc_matrix_path = self.par.doc_matrix_compaction_with_len_path
        else:
            print("Wrong leakage Type.")
            return

        self.dataset = []

        if self.par.use_data_dir == True:
            total_num_records = 0
            for fn in os.listdir(self.par.dataset_path):
                f_path = self.par.dataset_path + '/' + fn
                if f_path.endswith('.json'):
                    f_path_split = f_path.split("/")[-1].split("_")
                    print(f_path_split)
                    len_f_path_split = len(f_path_split)
                    if len_f_path_split == 3:
                        num_records = f_path_split[1]
                    elif len_f_path_split == 2:
                        num_records = f_path_split[0]
                    else:
                        print("Wrong file name when exportSimulatedLeakageFromDir")
                        return
                    total_num_records = total_num_records + int(num_records)
                    print(f_path)

            self.par.setNumRecords(total_num_records)
            self.genSimulatedLeakage(leakage_type)            
        else:
            print("Need to use_data_dir")
            return


    def exportSimulatedCompactionAnswer(self, leakage_type : LeakageType,
                                        experiment_num = 0):
        """The compaction leakage differs in that each unique field value can
        correspond to multiple (token) identifiers
        
        Generate a secret random mapping, mapping each field value to at most 5
        random identifiers.  Since the contention factor from 0 to 4 introduces
        small randomness, then each unique field value, corresponds to at most 5
        identifiers.
        
        Args:
          leakage_type : LeakageType
          experiment_num = 0
        
        Output files:
          answer_compaction : secret mapping beween each field value with some identifiers
        """
        
        self.answer_field_map = {}
        self.answer_field_l = {}

        with open(self.par.unique_fields_sorted_json_path, 'r') as infile:
            self.sorted_fields_data = json.load(infile)
            
        print("ExportSimulatedCompactionAnswer")

        # specifies different path
        if leakage_type == LeakageType.CompactionWithLen:
            answer_compaction_path = self.par.answer_compaction_with_len_path
        else:
            answer_compaction_path = self.par.answer_compaction_path


        """Main loop for generate the secret answer map:
        1. num_cf_per_field, the maximum tokens per field, 5 (default value)
        2. Create a (secret) random permutation of range(total_unique_fields * num_cf_per_field)
        3. Reserve -1 (empty string) to the first 5 values
        4. For each (sorted) field value for each field name, use such a mapping to create
        the answer map.
        """
        for fn_id, field_name in enumerate(self.par.export_field_name_list):
            rng = default_rng()
            total_unique_fields = len(self.sorted_fields_data[field_name]) + 1
            num_cf_per_field = self.par.contention_factor + 1
            field_id_l = range(total_unique_fields * num_cf_per_field)
            cf_fid_array = np.asarray(field_id_l)
            rng.shuffle(cf_fid_array)
            random_cf_fid_l = cf_fid_array.tolist()
            # print(f"random_cf_fid_l={random_cf_fid_l}")
            
            answer_field_map = {}
            # Use the last contention_factor number of entries
            empty_str_cf_fid_l = random_cf_fid_l[0:num_cf_per_field]
                
            # print(empty_str_cf_fid_l)
            answer_field_map[""] = [-1, empty_str_cf_fid_l]
            answer_field_l = [[-1, empty_str_cf_fid_l]]
            
            for f_id, field in enumerate(self.sorted_fields_data[field_name]):
                start_idx = (f_id + 1) * num_cf_per_field
                cf_fid_l = random_cf_fid_l[start_idx : start_idx + num_cf_per_field]
                answer_field_map[field] = [f_id, cf_fid_l]
                answer_field_l.append([f_id, cf_fid_l])
            self.answer_field_map[field_name] = answer_field_map
            self.answer_field_l[fn_id] = answer_field_l

        with open(answer_compaction_path, 'w') as outfile:
            outfile.write(f"{json.dumps(self.answer_field_l)}\n")
            outfile.write(f"{json.dumps(self.answer_field_map)}\n")
            
        # print("-------------------- Answer below --------------------")
        # print(f"{self.answer_field_l}\n")                
        # print(f"{self.answer_field_map}\n")
        print(f"Finished exporting compaction answer to {answer_compaction_path}.")
        # print(self.answer_field_map)
        # print(self.answer_field_l)        
        

    def exportQueryAnswer(self, use_simulated_leakage : bool = False,
                          leakage_type : LeakageType = LeakageType.Query,
                          experiment_num = 0):
        """Used as a reference answer to measure accuracy of the attack using query

        Args:
          use_simulated_leakage : bool = False,
          leakage_type : LeakageType = LeakageType.Query,
          experiment_num = 0

        Output files:
          answer_query: secret mapping beween each field value with some identifier
        """        
        
        self.answer_field_map = {}
        self.answer_field_l = {}

        if leakage_type == LeakageType.Query:
            answer_query_path = self.par.answer_query_path
        else:
            answer_query_path = self.par.answer_query_with_len_path            

        # Default insertion order, unique-ness
        with open(self.par.unique_fields_json_path, 'r') as infile:
            self.find_fields_data = json.load(infile)

        print(f"Experiment num = {experiment_num}-----------------------")

        # Shuffling the field values used in find queries
        for fn_id, field_name in enumerate(self.par.find_field_name_list):
            rng = default_rng()
            np_array = np.asarray(self.find_fields_data[field_name])
            rng.shuffle(np_array)
            # print(np_array)
            self.find_fields_data[field_name] = np_array.tolist()

        print(self.par.find_fields_json_path)

        with open(self.par.find_fields_json_path, 'w') as outfile:
            outfile.write(f"{json.dumps(self.find_fields_data)}\n")
            
        for fn_id, field_name in enumerate(self.par.find_field_name_list):
            answer_field_map = {}
            answer_field_map[""] = [-1, -1]
            answer_field_l = [[-1, [-1]]]
            indices, field_l_sorted = zip(*sorted(enumerate(self.find_fields_data[field_name]),
                                                  key=itemgetter(1)))
            for f_id, field in enumerate(field_l_sorted):
                answer_field_map[field] = [f_id, indices[f_id]]
                answer_field_l.append([f_id, [indices[f_id]]])
            self.answer_field_map[field_name] = answer_field_map
            self.answer_field_l[fn_id] = answer_field_l

        with open(answer_query_path, 'w') as outfile:
            outfile.write(f"{json.dumps(self.answer_field_l)}\n")
            outfile.write(f"{json.dumps(self.answer_field_map)}\n")
            
        # print("-------------------- Answer below --------------------")
        # print(f"{self.answer_field_l}\n")
        # print(f"{self.answer_field_map}\n")
        print(f"Finished exporting query answer to {answer_query_path}.")
        
    def checkCorrectness(self, leakage_type : LeakageType):
        """Check the correctness of the leakage profile

        Args:
          leakage_type : LeakageType
        """     
        
        if USE_COMPRESSION:
            doc_matrix_gz_path = f"{self.doc_matrix_path}.gz"
            with gzip.open(doc_matrix_gz_path, 'r') as f_in, open(self.doc_matrix_path, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)            
        
        if leakage_type in [LeakageType.CompactionWithLen, LeakageType.Compaction]:
            checkCompactionLeakageCorrectness(self.par, self.doc_matrix_path)
        else:
            checkQueryLeakageCorrectness(self.par, self.doc_matrix_path)
