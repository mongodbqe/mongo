"""file: src/client.py
This file provides client-slide functionality, such as initialization, insertion and find queries.
Also provides functions that help to analyze the tokens communicated between client and server.
"""
from pymongo import MongoClient
from pymongo.encryption_options import AutoEncryptionOpts
from pymongo.encryption import ClientEncryption
import os
import pprint

from parameters import *
import bson.binary
from bson import SON, BSON

from contextlib import redirect_stdout
import io  ## redirect stdout
import base64
import sys
import time
import timeit
from progress.bar import IncrementalBar
from pathlib import Path

class Client:
    """class Client:
    Handles client-side functionality
    """
    
    def __init__(self, par = Parameter(), mongo_task = MongoTask.CollectOplog):
        """Initialize the client 

        Args:
          par: configuration parameters
          mongo_task: MongoTask, default CollectOplog

        Returns:
          None
        """
        self.par = par
        self.encrypted_fields_map = createEncryptedFieldsInitMap(par)
        self.mongo_task = mongo_task
        self.connect()

    def connect(self):
        """Connect unencrypted_client and secure_client
        """
        self.initUnencryptedClient()
        self.initSecureClient()
        
    def setParameter(self, par : Parameter):
        """Load encrypted fields map to pass to the secure encryption's auto-encryption opts
        """
        self.par = par
        
    def loadEncryptedFieldsMap(self):
        """Load encrypted fields map to pass to the secure encryption's auto-encryption opts
        """
        print("Load EncryptedFieldsMap!")
        self.keyvault_client = self.unencrypted_client[KEY_VAULT_DB][KEY_VAULT_COLL]
        encrypted_db_coll_name = f"{self.par.default_encrypted_db_name}.{self.par.default_encrypted_coll_name}"
        print(encrypted_db_coll_name)
        for i in range(self.par.num_encrypted_fields):
            key_alt_name_str = f"dataKey{i + 1}"
            data_key_id = self.keyvault_client.find_one({"keyAltNames": key_alt_name_str})["_id"]
            self.encrypted_fields_map[encrypted_db_coll_name]["fields"][i]["keyId"] = data_key_id
        
    def initUnencryptedClient(self):
        """Initialize unencrypted client, and load encryptedFieldsMap
        """
        print("initUnencryptedClient")
        self.unencrypted_client = MongoClient(self.par.connection_str)
        self.loadEncryptedFieldsMap()
        
    def initSecureClient(self):
        """Initialize the secure client
        1. Load master key
        2. Initialize QE auto_encryption object
        3. Drop and re-create encrypted document collection (for freshness)
        """
        with open(MASTER_KEY_PATH, "rb") as f:
            local_master_key = f.read()
            kms_providers = {
                "local": {
                    "key": local_master_key  
            },
        }
        
        auto_encryption = AutoEncryptionOpts(
            kms_providers,
            KEY_VAULT_NAMESPACE,
            encrypted_fields_map=self.encrypted_fields_map,
            schema_map=None,
            crypt_shared_lib_path= CRYPT_SHARED_LIB_PATH_STR
        )
        self.secure_client = MongoClient(self.par.connection_str, auto_encryption_opts=auto_encryption)
        
        if self.mongo_task == MongoTask.CollectUnencryptedStats:
            self.unencrypted_client["unencrypted"].drop_collection("test")
            self.unencrypted_client["unencrypted"].create_collection("test")
            self.unencrypted_coll = self.unencrypted_client["unencrypted"]["test"]
        elif self.mongo_task in [MongoTask.CollectOplog, MongoTask.CollectQuerylog]:
            self.secure_client[self.par.default_encrypted_db_name].drop_collection(self.par.default_encrypted_coll_name)
            self.secure_client[self.par.default_encrypted_db_name].create_collection(self.par.default_encrypted_coll_name)
        else:
            pass ## assert the encrypted_coll exists
            
        self.encrypted_coll = self.secure_client[self.par.default_encrypted_db_name][self.par.default_encrypted_coll_name]

    def checkDocuments(self):
        """Check whether self.par.find_fields_json_path exists,
        the (randomzied) field value used in searches
        """
        if not Path(self.par.find_fields_json_path).exists():
             raise RuntimeError(f'Failed to open file {self.par.find_fields_json_path}')

    def insertDocumentsFromJSON(self):
        """Insert documents from a single JSON file
        Extract the num_records specified in the file name
        """
        with open(self.par.dataset_json_path, 'r') as infile:
            f_path = self.par.dataset_json_path.split("/")[-1]
            f_path_split = f_path.split("_")
            len_f_path_split = len(f_path_split)
            if len_f_path_split == 3:
                num_records = int(f_path_split[1])
            elif len_f_path_split == 2:
                num_records = int(f_path_split[0])
            else:
                print("Run file name in InsertDocumentsFromJSON.")
                return
            
            with IncrementalBar(f'Insert {f_path}', max=num_records,
                                suffix='%(percent).1f%% - %(eta)ds') as bar:
                for line in infile:
                    json_data = json.loads(line)
                    # print(json_data)
                    self.encrypted_coll.insert_one(json_data)
                    bar.next()
        print(f"num_records={num_records}")
        self.par.setNumRecords(num_records)            

    def insertDocumentsFromJSONDir(self):
        """Insert documents from the directory, and update the num_records
        """
        self.setLogLevel(1, "command")
        json_data_path = f"../dataset/{self.par.dataset_name}/{self.par.dataset_year}"

        num_records = 0
        
        for fn in os.listdir(json_data_path):
            f_path = json_data_path + '/' + fn
            if f_path.endswith('.json'):
                with open(f_path, 'r') as infile:
                    json_l = json.load(infile)
                    with IncrementalBar(f'Inserting {fn}', max=len(json_l),
                                        suffix='%(percent).1f%% - %(eta)ds') as bar:
                        for json_data in json_l:
                            self.encrypted_coll.insert_one(json_data)
                            bar.next()
                    num_records = num_records + len(json_l)
        print(f"num_records={num_records}")
        self.par.setNumRecords(num_records)

    def insertUnencryptedDocumentsFromJSONDir(self):
        """Insert documents from the directory, and update the num_records
        """
        json_data_path = f"../dataset/{self.par.dataset_name}/{self.par.dataset_year}"
        num_records = 0
        
        for fn in os.listdir(json_data_path):
            f_path = json_data_path + '/' + fn
            if f_path.endswith('.json'):
                with open(f_path, 'r') as infile:
                    json_l = json.load(infile)
                    # self.unencrypted_coll.insert_many(json_l)
                    with IncrementalBar(f'Inserting {fn}', max=len(json_l),
                                        suffix='%(percent).1f%% - %(eta)ds') as bar:
                        for json_data in json_l:
                            self.unencrypted_coll.insert_one(json_data)
                            bar.next()
                    num_records = num_records + len(json_l)
        print(f"num_records={num_records}")
        self.par.setNumRecords(num_records)        

    def findDocumentsUseFieldsJSON(self, fields_json_path : str):
        """Issues find queries using the (randomized) find queries for the field value

        Args:
          fields_json_path : str, specifies the field values used in the find queries

        Returns:
          None
        """
        with open(fields_json_path, 'r') as infile:
            data = json.load(infile)
        it = 0
        
        for field_name in self.par.find_field_name_list:
            for field in data[field_name]:
                result_cursor = self.encrypted_coll.find({field_name: field})
                for e in result_cursor:
                    pass
                it += 1
        
    def findDocumentsUseUniqueFieldsJSON(self):
        """Perform find on each unique field across all fields
        """
        print(self.par.unique_fields_json_path)
        with open(self.par.unique_fields_json_path, 'r') as infile:
            data = json.load(infile)
            
        it = 0
        
        for field_name in self.par.find_field_name_list:
            for field in data[field_name]:
                result_cursor = self.encrypted_coll.find({field_name: field})
                for e in result_cursor:
                    pass
                it += 1
                
    def insertOneDummyDocument(self):
        """Dummy insertion, used for analyzing the tokens sent in the payload
        """
        self.encrypted_coll.insert_one(
            {
                "drugname": "Dummy drugname",
                "drugnamewithstrength": "Dummy drugname 100 mg",
                "drug": "Dummy drug",
                "mmefactor": "1",
                "drugclass": "Opioid"
            }
        )

    def findOneDummyDocument(self):
        """Dummy find, used for analyzing the tokens sent in the payload
        """
        pprint.pprint(self.encrypted_coll.find_one({"drugname": "TRAMADOL HCL"}))

   
    def setLogLevel(self, log_level : int = DEFAULT_LOG_LEVEL, component : str = "command"):
        """Set log level = 1 also for secure_client

        Args:
          log_level : int = DEFAULT_LOG_LEVEL, the default logging verbosity level for querylog is 1
          component : str = "command", set the "command" component, verbosity level to 1

        Returns:
          None
        """
        self.unencrypted_client.admin.command(SON([
            ("setParameter", 1),
            ("logComponentVerbosity", {
                "command" : {
                    "verbosity": 1
                }}
             )]))
        print(f"Set log {component} level to {log_level}.")

    def parseQueryBSON(self, raw_str : str, field_name : str = 'drugname',
                       op : OpType = OpType.Insert):
        """parseQueryBSON is used for analyzing the tokens sent in the payload

        Args:
          raw_str : str
          field_name : str = 'drugname'
          op : OpType = OpType.Insert

        Returns:
           None
        """
        query_str_l = raw_str.splitlines()
        for query_str in query_str_l:
            if query_str.startswith('b'):
                bytes_str = bytes(query_str[2:-1], 'utf-8').decode("unicode_escape")
                data_bytes = bytes(bytes_str, 'raw_unicode_escape')
                query_bson = bson.BSON.decode(data_bytes)

                symbol_l = []
                field_bson = {}

                if op == OpType.Insert:
                    field_binary = query_bson['documents'][0][field_name][1:]
                    field_bson = bson.BSON.decode(field_binary)
                    symbol_l = ['d', 's', 'c', 'e']
                    
                elif op == OpType.Find:
                    field_binary = query_bson['filter'][field_name]['$eq'][1:]
                    field_bson = bson.BSON.decode(field_binary)
                    symbol_l = ['cm', 'd', 's', 'c']

                for symbol in symbol_l:
                    val = field_bson[symbol]
                    if type(val) == bytes:
                        val = base64.b64encode(val)
                    print(f"{symbol}: {val}")
                print('-----------')


    def collectTokenData(self):
        """Collect token data information, printing all components
        """
        self.setLogLevel(1)
        f = io.StringIO()
        with redirect_stdout(f):
            for i in range(5):
                self.insertOneDummyDocument()
        s = f.getvalue()                
        self.parseQueryBSON(s, 'drugname', OpType.Insert)
        f.close()
        
        f = io.StringIO()
        with redirect_stdout(f):
           self.findOneDummyDocument()
        s = f.getvalue()
        self.parseQueryBSON(s, 'drugname', OpType.Find)
        
    def collectFindQueryLogData(self):
        """Assume documents are inserted, and find on FIND_FIELD_NAME_LIST
        """
        self.findDocumentsUseFieldsJSON(self.par.find_fields_json_path)
        
    def closeConnection(self):
        """Close unencrypted and encrypted clients
        """
        self.unencrypted_client.close()
        self.secure_client.close()        

    def compact(self):
        """Client initializes `compaction` command
        """        
        encrypted_db_name = self.par.default_encrypted_db_name
        encrypted_coll_name = self.par.default_encrypted_coll_name
        encrypted_db = self.secure_client[encrypted_db_name]
        print(f"{encrypted_db_name}, {encrypted_coll_name}")
        print(encrypted_db.command({"compactStructuredEncryptionData": encrypted_coll_name}))
    
if __name__ == '__main__':
    client = Client()

    print(f"Start inserting")
    client.setLogLevel(1, "command")
    insert_time_str = timeit.Timer(client.insertDocumentsFromDir).timeit(number=1)
    print(f"Finished insertion: {insert_time_str}")
