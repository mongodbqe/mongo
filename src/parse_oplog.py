"""src/parse_oplog.py
Implement the functionality that takes as input the raw oplog, and extract
the leakage profile, stored in the doc_matrix/compaction, then check its correctness.
"""
import os
import json
import sys

from datetime import datetime ## timestamp
from parameters import Parameter, QE_ENC_FIELD_LEN_OFFSET, DEFAULT_CONTENTION_FACTOR
from collections import defaultdict, OrderedDict
import numpy as np # for matrix
import base64
import timeit
from pathlib import Path
import pandas as pd
from progress.bar import IncrementalBar
import time
from memory_profiler import profile
from Parser import Parser
from correctness import checkCompactionLeakageCorrectness

IF_CHECK_CORRECTNESS = True

class OpLogParser(Parser):
    
    def __init__(self, parameter : Parameter):
        """Initialization, create directories, 'info' is optional
        """
        self.par = parameter
        dir_name_l = ['doc_matrix/compaction', 'answer/compaction', 'info']
        for dir_name in dir_name_l:
            Path(f"../{dir_name}/{self.par.dataset_name}/{self.par.dataset_year}").mkdir(parents=True, exist_ok=True)

    def run(self):
        """
        1. Parse the oplog and extract the leakage
        2. Write the leakage to the file
        3. Check its correctness
        """
        self.parse()
        self.writeInfoToFile()
        if IF_CHECK_CORRECTNESS:
            check_correctness_str = timeit.Timer(self.checkCorrectness).timeit(number=1)
            print(f"Finished checkCorrectness: {check_correctness_str:.4} seconds.")

  
    def computeFieldEq(self, esc_cf_compaction_set : set, esc_cf_insertion_l : list) -> tuple:
        """Compute field equality based on ESC token intersection. Since each doc
          has only 6 encrypted fields at most, then loop through is faster than converting to a set.
        
        Args:
          esc_cf_compaction_set: esc_cf_compaction tokens
          esc_cf_insertion_l: esc_cf_insertion tokens
        
        Returns:
          the field equality on the two esc_cf_token_l, equal: (1, fieldname idx), not equal: 0
        """
        res = (0, 0, 0)
        for idx, esc_cf in enumerate(esc_cf_insertion_l):
            if esc_cf in esc_cf_compaction_set:
                res = (1, idx, esc_cf)
                break
        return res


    def addTxnTokenToMMap(self, doc : dict, data_command: dict, out_mmap : defaultdict):
        """Add transaction number, data w/o cf token to the map, and also update the ordered_dict
        - Order on the transaction number
        """
        
        token = doc["_id"]["$binary"]["base64"]
        txn = self.getTxnFromDataCommand(data_command)
        out_mmap[txn].append(token)

    def updateFindTxnTokenMMap(self, in_mmap : defaultdict):
        """Once the operation is committed and update the txn token map
        """
        for key, val in in_mmap.items():
            self.find_txn_esc_cf_mmap[key] = val

 
    def getTxnFromDataCommand(self, data_command : dict) -> str:
        """Get the txnuuid with txnnumber
        Args:
          data_command: from the oplog
        Returns:
          str: concatenation of txnuuid + txnNumber
        """
        txnuuid = -1
        txn_number = -1
        
        if "lsid" in data_command:
            data_lsid = data_command["lsid"]
            if "txnUUID" in data_lsid:
                txnuuid = data_lsid["txnUUID"]["$binary"]["base64"]
        else:
            raise ValueError("error: txnuuid is missing in the oplog.")
        if "txnNumber" in data_command:
            txn_number = data_command["txnNumber"]
        else:
            raise ValueError("error: txnNumber is missing in the oplog.")
        return f"{txnuuid}-{txn_number}"

    def genAnswerFieldMap(self):
        """Generate an answer field map.
    
        1. Parse doc_id_esc_cf_mmap and plaintext: for each fieldname, creates a
        mapping from the id of sorted unique field to a set of ids of the insertion
        esc_cf tokens following the insertion order.

        2. doc_id starts from 0.

        Reuse the compaction answer for with length/ without length leakage (due to scalability issue)
        Data_json is divided into several files
        """
        
        print("GenAnswerFieldMap")
        
        self.answer_field_map = {}
        self.answer_field_l = {}
        self.doc_id_field_length_leakage_mmap = defaultdict(list)
        
        fields_sorted_data = {}
        fields_sorted_len_leakage = {}

        with open(self.par.unique_fields_sorted_json_path, 'r') as infile:
            fields_sorted_data = json.load(infile)

        if self.par.dataset_name == "acs":
            with open(self.par.aux_info_len_leakage_path, 'r') as infile:
                json_l = []
                for line in infile:
                    json_l.append(json.loads(line))
                fields_sorted_len_leakage = json_l[0]
            
        #for idx, field_name in enumerate(self.par.export_field_name_list):
        # len_leakage_dict = fields_sorted_len_leakage[f"{idx}"]

        doc_id = 0
        sorted_insertion_id_mmap_l = []
        
        for i in range(self.par.num_encrypted_fields):
            sorted_insertion_id_mmap_l.append(defaultdict(set))
            # sorted_insertion_id_mmap_l[i][-1] = {-1}
            
        json_l = []

        print(self.par.dataset_json_path)

        if self.par.use_data_dir == False:
            with open(self.par.dataset_json_path, 'r') as infile:
                json_l = json.load(infile)
        else:
            data_dir = self.par.dataset_path
            for fn in os.listdir(data_dir):
                f_path = data_dir + '/' + fn
                if f_path.endswith('.json'):
                    with open(f_path, 'r') as infile:
                        for line in infile:
                            json_l.append(json.loads(line))

        for doc_id, json_data in enumerate(json_l):
            for fn_idx, fn in enumerate(self.par.export_field_name_list):
                inserted_field = json_data[fn]
                sorted_field_id = -1
                if inserted_field != '':
                    sorted_field_id = fields_sorted_data[fn].index(inserted_field)
                # print(f"inserted_field={inserted_field}, sorted_field_id={sorted_field_id}")
                esc_cf = self.doc_id_esc_cf_mmap[doc_id][fn_idx]
                esc_cf_id = self.doc_id_unique_fields_map[doc_id][fn_idx]

                if self.par.dataset_name == "acs":
                    inserted_field_length_leakage = fields_sorted_len_leakage[f"{fn_idx}"][f"{sorted_field_id}"]
                    self.doc_id_field_length_leakage_mmap[doc_id].append(inserted_field_length_leakage)
                
                if sorted_field_id not in sorted_insertion_id_mmap_l[fn_idx]:
                    sorted_insertion_id_mmap_l[fn_idx][sorted_field_id] = {esc_cf_id}
                else:
                    sorted_insertion_id_mmap_l[fn_idx][sorted_field_id].add(esc_cf_id)
            doc_id = doc_id + 1

        assert(len(json_l) == self.par.dataset_num_records)
        # print(sorted_insertion_id_mmap_l)

        output_dict = {}
        
        for idx, mmap in enumerate(sorted_insertion_id_mmap_l):
            temp_l = []
            for sorted_id, esc_cf_id_l in mmap.items():
                temp_l.append([sorted_id, sorted(list(esc_cf_id_l))])
            output_dict[idx] = temp_l
            
        # print(output_dict)
        with open(self.par.answer_compaction_path, 'w') as outfile:
            outfile.write(f"{json.dumps(output_dict)}\n")

        print(f"Finished exporting to {self.par.answer_compaction_path}")

  
    def genFieldInfo(self):
        """Generate Field equality vector by passing
          For each compaction esc_cf token, assign a unique id
        """
        
        ctr_unique_fields_l = [0] * self.par.num_encrypted_fields
        self.info_map = {}
        self.doc_id_unique_fields_map = {}

        self.token_unique_field_name_l = []
        
        for i in range(self.par.num_encrypted_fields):
            self.token_unique_field_name_l.append({})

        #print(f"len.compact_txn_esc_cf_mmap={len(self.compact_txn_esc_cf_mmap)}")
        #print(f"doc_id_esc_cf_mmap={len(self.doc_id_esc_cf_mmap)}")

        with IncrementalBar(f'genFieldInfo', max=len(self.compact_txn_esc_cf_mmap.items()),
                            suffix='%(percent).1f%% - %(eta)ds') as bar:
            for txn, esc_cf_compaction_list in self.compact_txn_esc_cf_mmap.items():
                fn_id = -1
                ctr_unique_field = -1
                is_fn_idx_set = False

                for esc_cf in esc_cf_compaction_list:
                    if esc_cf in self.esc_cf_doc_id_f_id_mmap:
                        pair = self.esc_cf_doc_id_f_id_mmap[esc_cf]
                        doc_id = pair[0]
                        fn_id = pair[1]
                        if is_fn_idx_set == False:
                            is_fn_idx_set = True
                            ctr_unique_field = ctr_unique_fields_l[fn_id]
                            self.token_unique_field_name_l[fn_id][esc_cf] = ctr_unique_field
                            ctr_unique_fields_l[fn_id] = ctr_unique_field + 1

                        if doc_id not in self.doc_id_unique_fields_map:
                            self.doc_id_unique_fields_map[doc_id] = [-1] * self.par.num_encrypted_fields

                        self.doc_id_unique_fields_map[doc_id][fn_id] = ctr_unique_field
                bar.next()
        # print(self.doc_id_unique_fields_map)
        # print(self.token_unique_field_name_l)
        # print(ctr_unique_fields_l)
        # print(self.info_map)

 
    def parse(self):
        """Parse the oplog:
        
        Assumptions:
          1. The order of the tokens used for accessing is preserved.
          2. Only one compaction is performed.
        
        For each doc insertion, each field's esc_cf_token is different since the key
        for each field name is different.
        
        The insertion of esc_cf_token is ordered using the encrypted_fieldnames
        """
        
        encrypted_db_name = self.par.default_encrypted_db_name
        encrypted_coll_name = self.par.default_encrypted_coll_name
        
        print(f"Start parsing oplog of {encrypted_db_name}.{encrypted_coll_name}:")
        
        is_compaction_started = False
        is_compaction_txn_set = False
        compaction_txnuuid = ""

        ns_ecoc = f"{encrypted_db_name}.enxcol_.{encrypted_coll_name}.ecoc"
        ns_esc = f"{encrypted_db_name}.enxcol_.{encrypted_coll_name}.esc"
        ns_doc = f"{encrypted_db_name}.{encrypted_coll_name}"

        """In the format of doc_id_esc_cf_mmap[doc_id], a list of lists

        """
        self.doc_id_esc_cf_mmap = defaultdict(list)
        self.doc_id_field_length_mmap = defaultdict(list)
        self.esc_cf_doc_id_f_id_mmap = defaultdict(list)
        # self.compact_txn_esc_cf_mmap = defaultdict(set)
        self.compact_txn_esc_cf_mmap = defaultdict(list)        
        # Defer adding to the compact mmap due to insertion also has compact operation
        # Once the transaction is committed, and add to "compact" operation
        op_commit_txn_flag = False

        """Parse doc insertion tokens:

        1. For each doc insertion, the oplog works as follows, for each
        encrypted fieldname, perform one ESC insertion, followed by an ECOC
        insertion; finally an doc access to doc collection.

        2. Add length information.

        """
        doc_id = 0
        print("Parse_oplog!!!!!!!!!!")
        with IncrementalBar(f'Parse Oplog', max=self.par.oplog_num_ops,
                            suffix='%(percent).1f%% - %(eta)ds') as bar:
            with open(self.par.oplog_path, 'r') as infile:
                for line in infile:
                    data = json.loads(line)
                    # print(f"{data}\n")
                    txn_current = ""
                    if "lsid" in data:
                        txn_current = self.getTxnFromDataCommand(data)
                        if is_compaction_started:
                            data_lsid = data["lsid"]
                            if is_compaction_txn_set == False:
                                if "txnUUID" in data_lsid:
                                    txnuuid = data_lsid["txnUUID"]["$binary"]["base64"]
                                compaction_txnuuid = txnuuid
                                is_compaction_txn_set = True

                    if "o" in data:
                        data_o = data["o"]
                        if "renameCollection" in data_o:
                            if data_o["renameCollection"] == ns_ecoc:
                                is_compaction_started = True
                                # print("\n renameCollection -> Compaction")
                        if "drop" in data_o:
                            if data_o["drop"] == f"enxcol_.{encrypted_coll_name}.ecoc.compact":
                                # print(f"\n Drop enxcol_.{encrypted_coll_name}.ecoc.compact")
                                if is_compaction_started == True:
                                    is_compaction_started = False

                        if "applyOps" in data_o:
                            data_ops = data_o["applyOps"]

                            is_doc_insertion = False
                            esc_cf_token_l = []

                            for data_op in data_ops:

                                if "op" in data_op and "ns" in data_op:
                                    op_type = data_op["op"]
                                    ns = data_op["ns"]

                                    # print(f"{op_type}, {ns}, data_op["o"]["_id"]")
                                    """insert to a esc_cf_token_l if is not compaction
                                    """
                                    if is_compaction_started:
                                        """
                                        op_val_test = data_op["o"]
                                        esc_cf_token_test = op_val_test["_id"]["$binary"]["base64"]
                                        print(f"{op_type}, {ns}, {esc_cf_token_test}")
                                        print(f"{op_val_test}")
                                        """

                                        if op_type == "d" and ns == ns_esc:
                                            if "o" in data_op:
                                                op_val = data_op["o"]
                                                esc_cf_token = op_val["_id"]["$binary"]["base64"]
                                                if not txn_current == "":
                                                    if txn_current not in self.compact_txn_esc_cf_mmap:
                                                        #self.compact_txn_esc_cf_mmap[txn_current] = set([esc_cf_token])
                                                        self.compact_txn_esc_cf_mmap[txn_current] = [esc_cf_token]
                                                    else:
                                                        #self.compact_txn_esc_cf_mmap[txn_current].add(esc_cf_token)
                                                        self.compact_txn_esc_cf_mmap[txn_current].append(esc_cf_token)
                                    else:
                                        if op_type == "i":
                                            if ns == ns_esc:
                                                if "o" in data_op:
                                                    op_val = data_op["o"]
                                                    esc_cf_token = op_val["_id"]["$binary"]["base64"]
                                                    esc_cf_token_l.append(esc_cf_token)
                                            elif ns == ns_doc:
                                                if "o" in data_op:
                                                    op_val = data_op["o"]
                                                    if is_doc_insertion == False:
                                                        if doc_id not in self.doc_id_esc_cf_mmap:
                                                            self.doc_id_esc_cf_mmap[doc_id] = []
                                                            for f_id, esc_cf in enumerate(esc_cf_token_l):
                                                                self.doc_id_esc_cf_mmap[doc_id].append(esc_cf)
                                                                self.esc_cf_doc_id_f_id_mmap[esc_cf] = [doc_id, f_id]
                                                            is_doc_insertion = True
                                                        else:
                                                            raise ValueError("error: same doc is inserted at least twice.")
                                                        for idx, fieldname in enumerate(self.par.export_field_name_list):
                                                            if fieldname in op_val:
                                                                efield = op_val[fieldname]["$binary"]["base64"]
                                                                efield_len = len(base64.b64decode(efield))
                                                                self.doc_id_field_length_mmap[doc_id].append(efield_len)
                                                            else:
                                                                raise ValueError(f"error: {fieldname} does not exist in doc.")
                                                    doc_id = doc_id + 1
                    bar.next()                                                
        # print(f"compaction txnuuid={compaction_txnuuid}")
        # print(self.doc_id_esc_cf_mmap)
        # print(self.doc_id_field_length_mmap)
        # print(self.compact_txn_esc_cf_mmap)

    def dumpDocidEsccfmmap(self):
        return self.doc_id_esc_cf_mmap
        

    """This is optional and is not used.
    """
    def genContentionInfo(self):
        """Contention info (optional)
        """
        with open(self.par.info_path, 'w') as outfile:
            outfile.write(f"{json.dumps(self.info_map)}\n")
        print(f"Finished exporting to {self.par.info_path}")

    
    def genDocMatrix(self):
        doc_matrix_outfile = open(self.par.doc_matrix_compaction_path, 'w')

        if self.par.dataset_name == "acs":
            doc_matrix_with_len_outfile = open(self.par.doc_matrix_compaction_with_len_path, 'w')
            
        # print(self.doc_id_field_length_leakage_mmap)
        
        """Insertion: doc_id is the same as the insertion order.
        """
        for doc_id, unique_field_id_l in self.doc_id_unique_fields_map.items():
            doc_field_json = {}
            doc_field_json['i'] = doc_id
            doc_field_len_json = {}
            doc_field_len_json['i'] = doc_id            
            
            u_fid_l_key = f"{unique_field_id_l}"
            
            if u_fid_l_key not in self.info_map:
                self.info_map[u_fid_l_key] = 1
            else:
                self.info_map[u_fid_l_key] = self.info_map[u_fid_l_key] + 1

            for fn_idx, field_esc_cf_id in enumerate(unique_field_id_l):
                
                doc_field_json[fn_idx] = [field_esc_cf_id,
                                          self.doc_id_field_length_mmap[doc_id][fn_idx]]

                if self.par.dataset_name == "acs":
                    doc_field_len_json[fn_idx] = [field_esc_cf_id,
                                                  self.doc_id_field_length_leakage_mmap[doc_id][fn_idx]]
                
            doc_matrix_outfile.write(f"{json.dumps(doc_field_json)}\n")
            
            if self.par.dataset_name == "acs":
                doc_matrix_with_len_outfile.write(f"{json.dumps(doc_field_len_json)}\n")            
            
        doc_matrix_outfile.close()
        print(f"Finished exporting to {self.par.doc_matrix_compaction_path}")
        
        if self.par.dataset_name == "acs":
            doc_matrix_with_len_outfile.close()
            print(f"Finished exporting to {self.par.doc_matrix_compaction_with_len_path}")

    def writeInfoToFile(self):
        time_a = time.perf_counter()
        self.genFieldInfo()
        time_b = time.perf_counter()
        print(f"Total genFieldInfo time:{time_b - time_a:.4} seconds.")
        self.genAnswerFieldMap()
        time_c = time.perf_counter()
        print(f"Total genAnswerFieldmap time:{time_c - time_b:.4} seconds.")
        self.genDocMatrix()
        time_d = time.perf_counter()
        print(f"Total genDocMatrix time:{time_d - time_c:.4} seconds.")

    def checkCorrectness(self):
        checkCompactionLeakageCorrectness(par=self.par, doc_matrix_path=self.par.doc_matrix_compaction_path)
        checkCompactionLeakageCorrectness(par=self.par, doc_matrix_path=self.par.doc_matrix_compaction_with_len_path)
