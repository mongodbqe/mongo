"""file: mongo/src/make_data_key.py

This file interacts with MongoDB QE, and is used to initialize encrypted
document collection.

"""

import base64
import os

from pymongo import MongoClient, ASCENDING
from pymongo.encryption_options import AutoEncryptionOpts
from pymongo.encryption import ClientEncryption, MongoCryptOptions

from bson.codec_options import CodecOptions
from bson.binary import STANDARD, UUID

from parameters import *

def makeDataKey(par = Parameter()):
    """Initialize data encryption key
    Args:
      par: configuration parameters

    Returns:
      None
    """
    
    with open(MASTER_KEY_PATH, "rb") as f:
        local_master_key = f.read()

    # Use `local` master key
    kms_providers = {
        "local": {
            "key": local_master_key  # local_master_key variable from the previous step
        },
    }

    key_vault_client = MongoClient(par.connection_str)
    key_vault_client.drop_database(KEY_VAULT_DB)

    key_vault_client[KEY_VAULT_DB][KEY_VAULT_COLL].create_index(
        [("keyAltNames", ASCENDING)],
        unique=True,
        partialFilterExpression={"keyAltNames": {"$exists": True}},
    )

    client = MongoClient(par.connection_str)

    # intializes client side encryption
    client_encryption = ClientEncryption(
        kms_providers,  # pass in the kms_providers variable from the previous step
        KEY_VAULT_NAMESPACE,
        client,
        CodecOptions(uuid_representation=STANDARD),
    )

    encrypted_db_name = par.default_encrypted_db_name
    encrypted_coll_name = par.default_encrypted_coll_name
    encrypted_db_coll_name = f"{encrypted_db_name}.{encrypted_coll_name}"

    """Create encrypted fields map for QE (the initialization function is
     defined in src/parameters.py), this is essential for MongoDB QE 6.0 an
     encrypted fields map stores the information such as, field key id, field
     names, type of field value, and the type of supported QE operation, e.g.,
     equality search.
    """
    encrypted_fields_map = createEncryptedFieldsInitMap(par)

    # after initialization, fill in the values of the encrypted_fields_map
    for i in range(par.num_encrypted_fields):
        data_key_alt_name_str = f"dataKey{i + 1}"
        data_key_id = client_encryption.create_data_key(kms_provider="local",
                                                        key_alt_names=[data_key_alt_name_str])
        encrypted_fields_map[encrypted_db_coll_name]["fields"][i]["keyId"] = data_key_id
    
    # initialize the QE auto-encryption module, this requires the
    # encrypted_fields_map and crypt_shared_lib
    auto_encryption = AutoEncryptionOpts(
        kms_providers,
        KEY_VAULT_NAMESPACE,
        encrypted_fields_map=encrypted_fields_map,
        schema_map=None,
        crypt_shared_lib_path=CRYPT_SHARED_LIB_PATH_STR,
    )
    
    secure_client = MongoClient(par.connection_str, auto_encryption_opts=auto_encryption)
    secure_client.drop_database(encrypted_db_name)
    encrypted_db = secure_client[encrypted_db_name]
    encrypted_db.create_collection(encrypted_coll_name)
    print(f"Created encrypted collection: {encrypted_db_coll_name}!")

if __name__ == '__main__':
    makeDataKey()
