"""src/parse_query_log.py
Implement the functionality that takes as input the raw querylog, and extract
the leakage profile, stored in the doc_matrix/query, then check its correctness.
"""
import os
import json
import sys
from datetime import datetime ## timestamp
from collections import defaultdict, OrderedDict
from util import isValidUUID, fillLowerTriSymmetric
import numpy as np # for matrix
import base64
import timeit
from pathlib import Path
from parameters import *
from parse_oplog import OpLogParser
from progress.bar import IncrementalBar
from Parser import Parser
from correctness import checkQueryLeakageCorrectness
import tempfile

IF_CHECK_CORRECTNESS = True

class LogParser(Parser):
    """Extends base class Parser, tailored for parsing querylog
    """
    def __init__(self, par = Parameter()):
        self.par = par
        dir_name_l = ['doc_matrix/query']
        for dir_name in dir_name_l:
            Path(f"../{dir_name}/{self.par.dataset_name}/{self.par.dataset_year}").mkdir(parents=True, exist_ok=True)

    def run(self):
        self.checkDocuments()
        # Add extra length leakage
        super().getLengthLeakage(self.par.dataset_num_records)
        parse_query_time_str = timeit.Timer(self.parse).timeit(number=1)
        print(f"Finished parsing query log: {parse_query_time_str:.4} seconds.")
        gen_docmatrix_info_str = timeit.Timer(self.genDocMatrix).timeit(number=1)
        print(f"Finished genDocMatrix initial: {gen_docmatrix_info_str:.4} seconds.")
        gen_postProcessing_str = timeit.Timer(self.postProcessing).timeit(number=1)
        print(f"Finished genDocMatrix: {gen_postProcessing_str:.4} seconds.")

        if IF_CHECK_CORRECTNESS:
            check_correctness_str = timeit.Timer(self.checkCorrectness).timeit(number=1)
            print(f"Finished checkCorrectness: {check_correctness_str:.4} seconds.")
  
    def checkDocuments(self):
        """only use aux_info_len_leakage to add the length leakage
        """
        path_list = [self.par.unique_fields_sorted_json_path, self.par.aux_info_len_leakage_path,
                     self.par.snapshot_path]
        for path in path_list:
            # print(path)
            if not Path(path).exists():
                raise RuntimeError(f'Failed to open file {path}')

    def postProcessingSub(self, doc_matrix_path : str):
        doc_mat_query_file_in = open(doc_matrix_path, 'r')
        
        with tempfile.NamedTemporaryFile(mode="w+") as tmp:
            for line in doc_mat_query_file_in:
                data = json.loads(line)

                for fn_id, field_name in enumerate(self.par.export_field_name_list):
                    assigned_id = data[f"{fn_id}"][0]
                    # print(f"assigned_id={assigned_id}")
                    # print(f"num_unique_fields={self.num_unique_fields_l[fn_id]}")
                    if assigned_id == -1 and data[f"{fn_id}"][1] != QE_ENC_FIELD_LEN_OFFSET:
                        data[f"{fn_id}"][0] = self.most_freq_fid_l[fn_id]
                tmp.write(json.dumps(data) + '\n')

            doc_mat_query_file_in.close()
            tmp.seek(0)
            out_file = open(doc_matrix_path, 'w')

            for line in tmp:
                out_file.write(line)
            out_file.close()
            tmp.flush()

    def postProcessing(self):
        """Consistency postprocessing
        - For 3 million records: handling potential
        edge cases (may happen when the number of documents associated with
        particular fields are large):
        - For 300K, 30K records, post processing is not necessary.
        """
        
        # print(self.extracted_field_freq_l)

        num_encrypted_fields = len(self.par.export_field_name_list)
        self.most_freq_fid_l = []
        
        # compute the most frequent one.                
        for freq_l in self.extracted_field_freq_l:
            sort_index = np.flip(np.argsort(freq_l))
            self.most_freq_fid_l.append(sort_index[0].item())
            
        self.postProcessingSub(self.par.doc_matrix_query_path)
        self.postProcessingSub(self.par.doc_matrix_query_with_len_path)

    def computeFieldEq(self, x_txn : int, y_txn : int) -> int:
        """Compute field equality based on ESC look-up token intersection, txnid

        Args:
          x_txn: txnid
          y_txn: txnid

        Returns:
          the field equality on the two transaction, equal: 1, not equal: 0
        """
        
        x_set_f = self.find_txn_esc_cf_set_l[x_txn]
        y_set_f = self.find_txn_esc_cf_set_l[y_txn]
        intersect_set  = x_set_f & y_set_f
        
        if len(intersect_set) > 0:
            return 1
        else:
            return 0
        
    def genDocMatrix(self):
        """Generate doc information with length information encrypted document collection
        - The fieldid assigned to -1 can also tell from the log when the fieldname is missing.
        - Since we encrypt the empty string field, we assign the field with -1
        - by using the snapshot encrypted document collection.
        - Assume the consistency of the insertion order and the unique_fields order
        - Assert doc_id order in the snapshot, also the same as insertion order
        """
        json_snapshot_l = []
        
        with open(self.par.unique_fields_sorted_json_path, 'r') as infile:
            fields_data = json.load(infile)
            
        # according to the field_name listing
        self.num_unique_fields_l = []
        # compute frequency information
        self.extracted_field_freq_l = []
        for field_name in self.par.find_field_name_list:
            num_unique_fields = len(fields_data[field_name])
            self.num_unique_fields_l.append(num_unique_fields)
            self.extracted_field_freq_l.append([0] * num_unique_fields)

        field_name_num_unique_l = list(zip(self.par.find_field_name_list, self.num_unique_fields_l))
        # print(field_name_num_unique_l)
        # Remap to a simple doc_id
        # Doc id and field id all start from 0
        print(f"dataset_num_records={self.par.dataset_num_records}")
        # print(field_name_num_unique_l)
        # print(self.doc_id_field_length_leakage_mmap)
        
        doc_mat_query_file = open(self.par.doc_matrix_query_path, 'w')
        doc_mat_query_len_file = open(self.par.doc_matrix_query_with_len_path, 'w')
        
        doc_id = 0
        
        with IncrementalBar(f'genDocMatrix initial', max=self.par.dataset_num_records,
                            suffix='%(percent).1f%% - %(eta)ds') as bar:
            infile = open(self.par.snapshot_path, 'r')
            for i in range(self.par.dataset_num_records):
                line = infile.readline()
                doc = json.loads(line)
                doc_field_json = {}
                doc_field_len_json = {}
                doc_raw_id = doc["_id"]["$oid"]
                doc_field_json['i'] = doc_id
                doc_field_len_json['i'] = doc_id

                # print(doc)
                field_offset = 0
                mat_field_start_x = self.par.dataset_num_records + field_offset

                for idx, e in enumerate(field_name_num_unique_l):
                    field_name = e[0]
                    num_unique_fields = e[1]
                    mat_x = mat_field_start_x
                    unique_field_id = -1

                    for f_i in range(num_unique_fields):
                        mat_x = mat_field_start_x + f_i
                        # x_txn = self.txn_l[i]
                        # y_txn = self.txn_l[mat_x]
                        x_txn = i
                        y_txn = mat_x

                        if self.computeFieldEq(x_txn, y_txn):
                            unique_field_id = f_i
                            self.extracted_field_freq_l[idx][f_i] = self.extracted_field_freq_l[idx][f_i] + 1
                            break

                    if field_name in doc:
                        efield = doc[field_name]['$binary']['base64']
                        decoded_efield = base64.b64decode(efield)
                        decoded_efield_len = len(decoded_efield)
                        doc_field_json[idx] = [unique_field_id, decoded_efield_len]
                        field_id = self.par.export_field_name_list.index(field_name)
                        len_leak = self.doc_id_field_length_leakage_mmap[doc_id][field_id]
                        doc_field_len_json[idx] = [unique_field_id, len_leak]

                    mat_field_start_x = mat_field_start_x + num_unique_fields

                doc_mat_query_file.write(f"{json.dumps(doc_field_json)}\n")
                doc_mat_query_len_file.write(f"{json.dumps(doc_field_len_json)}\n")
                doc_id = doc_id + 1
                bar.next()
        infile.close()


    def addTxnTokenToMMap(self, doc : dict, data_command: dict, out_mmap : defaultdict):
        """genFieldEqualityMatrixOptimized(self) and genFieldEqualityMatrix(self)
        are not used, the equality is directly computed in the genDocMatrix)

        Generate field equality matrix for all operations
        - Initialize a square mat of size the same as operation length
        - The left entries are insertions and the right entries are find operations
        - Compute the upper-triangular equality
        
        Add transaction number, data w/o cf token to the map, and also update the ordered_dict
        - Order on the transaction number
        """
        
        token = doc["_id"]["$binary"]["base64"]
        txn = self.getTxnFromDataCommand(data_command)

        current_token_id = -1

        if token not in self.esc_cf_id_map:
            current_token_id = self.esc_id_ctr
            self.esc_cf_id_map[token] = current_token_id
            self.esc_id_ctr = self.esc_id_ctr + 1
        else:
            current_token_id = self.esc_cf_id_map[token]

        current_txn_id = -1

        if txn not in self.txn_id_map:
            current_txn_id = self.txn_id_ctr
            self.txn_id_map[txn] = current_txn_id
            self.txn_id_ctr = self.txn_id_ctr + 1
        else:
            current_txn_id = self.txn_id_map[txn]
        
        out_mmap[current_txn_id].add(current_token_id)
        

    def updateFindTxnTokenMMap(self, in_mmap : defaultdict):
        """Once the operation is committed and update the txn token map
        
        Args:
          in_mmap: key: transaction string, val is a set of esc_cf data tokens
        """
        for txn, esc_cf_set in in_mmap.items():
            self.find_txn_esc_cf_set_l[txn] = esc_cf_set
        
    def getTxnFromDataCommand(self, data_command : dict) -> str:
        """Get the txnuuid with txnnumber
        
        Args:
          data_command: from the log
        
        Returns:
          str concatenation of txnuuid + txnNumber
        """
        
        if "lsid" in data_command:
            txnuuid = data_command["lsid"]["txnUUID"]["$uuid"]
        else:
            raise ValueError("error: txnuuid is missing in the log.")
        if "txnNumber" in data_command:
            txn_number = data_command["txnNumber"]
        else:
            raise ValueError("error: txnNumber is missing in the log.")
        return f"{txnuuid}-{txn_number}"

    def parse(self):
        """Parse the query log with the order of the tokens preserved
        - Once the transaction is committed, add the find_txn_esc_cf tokens to the transaction
        - (Memory optimization): Keep an esc_cf token to id mapping
        - Safecontent is not used.
        """
        encrypted_db_name = self.par.default_encrypted_db_name
        encrypted_coll_name = self.par.default_encrypted_coll_name
        log_level = DEFAULT_LOG_LEVEL
        
        print("Start parsing")
        self.safe_content_doc_id_map = {}
        self.txn_l = []
        self.op_l = []
        temp_find_txn_esc_cf_mmap = defaultdict(set)
        self.esc_cf_id_map = {}
        self.esc_id_ctr = 0
        self.txn_id_ctr = 0
        self.txn_id_map = {}
        self.find_txn_esc_cf_set_l = []
        
        if log_level == 1:
            command_str = "command"
        elif log_level == 2:
            command_str = "commandArgs"
        else:
            raise ValueError("Log_level needs to be set 1 or 2.")

        op_commit_txn_flag = False
        txn_current = ""

        """Parse with the oplog is ommited.
        - Not supported, as query log (in its current state) does not contain all the find operations.
        """
        print(f"Open log_path={self.par.log_path}")
        total_log_size_bytes = os.stat(self.par.log_path).st_size
        num_lines_approximated = int(total_log_size_bytes / 910)
        txn_ctr = 0
        
        with IncrementalBar(f'Parse Querylog', max=num_lines_approximated,
                            suffix='%(percent).1f%% - %(eta)ds') as bar:
            with open(self.par.log_path, 'r') as infile:
                for line in infile:
                    data = json.loads(line)
                    # print(data)
                    # print(date_time)
                    # Get operation type
                    if "attr" in data:
                        data_attr = data["attr"]
                        
                        if "command" in data_attr:
                            data_command = data_attr["command"]
                            if "commitTransaction" in data_command:
                                # print(data_command)
                                if data_command["commitTransaction"] == 1:
                                    op_commit_txn_flag = True

                                txn = self.getTxnFromDataCommand(data_command)
                                txn_current = txn

                        if command_str in data_attr:
                            data_command = data_attr[command_str]

                            if "insert" in data_command:
                                if data_command["insert"] == encrypted_coll_name:
                                    # Toggle operation type
                                    # print(data_command)
                                    if op_commit_txn_flag == True:
                                        # self.txn_l.append(txn_current)
                                        # txn_ctr = txn_ctr + 1
                                        # self.op_l.append(OpType.Insert)
                                        self.find_txn_esc_cf_set_l.append(set([]))
                                        self.updateFindTxnTokenMMap(temp_find_txn_esc_cf_mmap)
                                        temp_find_txn_esc_cf_mmap.clear()
                                        op_commit_txn_flag = False
                                        txn_current = ""

                            elif "find" in data_command:
                                if data_command["find"] == encrypted_coll_name:
                                    # Toggle operation type
                                    # print(data_command)                                    
                                    if op_commit_txn_flag == True:
                                        # self.txn_l.append(txn_current)
                                        # txn_ctr = txn_ctr + 1
                                        # self.op_l.append(OpType.Find)
                                        self.find_txn_esc_cf_set_l.append(set([]))
                                        self.updateFindTxnTokenMMap(temp_find_txn_esc_cf_mmap)
                                        temp_find_txn_esc_cf_mmap.clear()
                                        op_commit_txn_flag = False
                                        txn_current = ""

                                elif data_command["find"] == f"enxcol_.{encrypted_coll_name}.esc":
                                    # print(data_command)
                                    doc = data_command["filter"]
                                    self.addTxnTokenToMMap(doc, data_command, temp_find_txn_esc_cf_mmap)
                                    # print(data_command_args)
                    bar.next()
                # print(json.dumps(self.find_txn_esc_cf_mmap, indent=4))
                # print(json.dumps(self.txn_doc_id_map, indent=4))
                # print(self.txn_l)
                # print(self.op_l)

                with open(self.par.unique_fields_sorted_json_path, 'r') as infile:
                    unique_fields_data = json.load(infile)

                self.num_unique_fields_l = []
                for field_name in self.par.find_field_name_list:
                    num_unique_fields = len(unique_fields_data[field_name])
                    self.num_unique_fields_l.append(num_unique_fields)
                    
                total_num_unique_fields = 0
                
                for num_unique_fields in self.num_unique_fields_l:
                    total_num_unique_fields = total_num_unique_fields + num_unique_fields
                    
 
                
    def checkCorrectness(self):
        checkQueryLeakageCorrectness(self.par, self.par.doc_matrix_query_path)
        checkQueryLeakageCorrectness(self.par, self.par.doc_matrix_query_with_len_path)
