import random
import numpy as np

import json
import time

import sys
import gzip
from pathlib import Path
import argparse

''' 
# global constants
# N_fields: total number of fields
# N_cand_fields_max: the maximum number of fields used in neighbourhood generation
# contention_factor: this is the same as cf_max+1 in the paper. Use contention_factor = 1 for attacking query log
# CF_buffer: a constant that affects how many identifiers are allowed to be assigned to a field value. It changes automatically
# N_iter_refine: the number of iterations to run simulated annealing on inidvidual fields
# N_iter_attack: the number of iterations to run simulated annealing on all fields
'''
N_fields = 6
N_cand_fields_max = 3
contention_factor = 1
CF_buffer = 0
N_iter_refine = 5*10**8
N_iter_attack = 10**4



'''
# Function to read the auxiliary information and the leakage
# For an attack without length leakage, the length in the auxiliary data and the leakage are set to a constant (same for all field values) 
'''
def read_files(file_name_aux, file_name_leakage):
    file_aux = open(file_name_aux, 'r')
    _text_lens_aux = json.loads(file_aux.readline())
    _tuples_aux = json.loads(file_aux.readline())
    file_aux.close()

    text_lens_aux = []
    for field_idx in _text_lens_aux:
        lens = {}
        for key in _text_lens_aux[field_idx]:
            lens[int(key)] = _text_lens_aux[field_idx][key]
        text_lens_aux.append(lens)

    tuples_aux = {}
    for key in _tuples_aux:
        tuple_key = key[1:-1].split(',')
        tuple_key = [int(x) for x in tuple_key]
        tuples_aux[tuple(tuple_key)] = _tuples_aux[key]


    tuples_obs = {}
    text_lens_obs = [{} for ii in range(N_fields)]
    file_leakage = gzip.open(file_name_leakage, 'rb')

    for line in file_leakage.readlines():
        document = json.loads(line)

        tuple_key = [int(document[str(ii)][0]) for ii in range(N_fields)]

        if tuple(tuple_key) not in tuples_obs:
            tuples_obs[tuple(tuple_key)] = 0
        tuples_obs[tuple(tuple_key)] += 1

        for ii in range(N_fields):
            if document[str(ii)][0] not in text_lens_obs[ii]:
                text_lens_obs[ii][document[str(ii)][0]] = document[str(ii)][1]
    file_leakage.close()


    return tuples_aux, text_lens_aux, tuples_obs, text_lens_obs


'''
# Generate N_queries uniformly distributed queries per field
'''
def generate_observed_data_uniform(tuples_raw, text_lens_raw, N_queries):
    tuples_obs, text_lens_obs = {}, []

    # generate queries
    queries = []
    for field_idx in range(N_fields):
        field_values = list(text_lens_raw[field_idx].keys())
        queries_field = random.choices(field_values, k=N_queries)
        queries.append(set(queries_field))


    # generate new tuple counts
    for tuple_key in tuples_raw:
        tuple_key_new = []
        for field_idx in range(N_fields):
            if tuple_key[field_idx] in queries[field_idx]:
                tuple_key_new += [tuple_key[field_idx]]
            else:
                tuple_key_new += [None]
                
        tuple_key_new = tuple(tuple_key_new)
        if tuple_key_new not in tuples_obs:
            tuples_obs[tuple_key_new] = 0

        tuples_obs[tuple_key_new] += tuples_raw[tuple_key]

    # generate new tuple lengths
    for field_idx in range(N_fields):
        text_lens_field = {}
        for field_key in text_lens_raw[field_idx].keys():
            if field_key in queries[field_idx]:
                text_lens_field[field_key] = text_lens_raw[field_idx][field_key]

        text_lens_obs += [text_lens_field]

    return tuples_obs, text_lens_obs


'''
# Generate N_queries Zipf distributed queries per field
'''
def generate_observed_data_zipf(tuples_raw, text_lens_raw, N_queries):
    tuples_obs, text_lens_obs = {}, []

    # generate queries, weights are determined by weights
    queries = []
    for field_idx in range(N_fields):
        field_values = list(text_lens_raw[field_idx].keys())

        random.shuffle(field_values)
        weights = np.array([1/(ii+1) for ii in range(len(field_values))])
        weights /= np.sum(weights)
        
        
        queries_field = random.choices(field_values, weights=weights, k=N_queries)
        queries_field += [-1]
        queries.append(set(queries_field))


    # generate new tuple counts
    for tuple_key in tuples_raw:
        tuple_key_new = []
        for field_idx in range(N_fields):
            if tuple_key[field_idx] in queries[field_idx]:
                tuple_key_new += [tuple_key[field_idx]]
            else:
                tuple_key_new += [None]
                
        tuple_key_new = tuple(tuple_key_new)
        if tuple_key_new not in tuples_obs:
            tuples_obs[tuple_key_new] = 0

        tuples_obs[tuple_key_new] += tuples_raw[tuple_key]

    # generate new tuple lengths
    for field_idx in range(N_fields):
        text_lens_field = {}
        for field_key in text_lens_raw[field_idx].keys():
            if field_key in queries[field_idx]:
                text_lens_field[field_key] = text_lens_raw[field_idx][field_key]

        text_lens_obs += [text_lens_field]

    return tuples_obs, text_lens_obs


'''
# Function to pre-compute the candidate set (which field values each of the leakage identifiers can be assigned to)
# The candidates of a field token must have the same length as the field token (due to length leakage)
# The candidates are chosen to be the field values that have similar frequencies as the field token (inference on frequency)
'''
def build_guess_candidates(tuples_aux, text_lens_aux, tuples_obs, text_lens_obs):
    candidates = []
    for ii in range(N_fields):
        tuple_aux_field = {}
        tuple_obs_field = {}
        N_aux, N_obs = 0, 0

        for tuple_key in tuples_aux:
            if tuple_key[ii] not in tuple_aux_field:
                tuple_aux_field[tuple_key[ii]] = 0
            tuple_aux_field[tuple_key[ii]] += tuples_aux[tuple_key]
            N_aux += tuples_aux[tuple_key]

        for tuple_key in tuples_obs:
            if tuple_key[ii] not in tuple_obs_field:
                tuple_obs_field[tuple_key[ii]] = 0
            tuple_obs_field[tuple_key[ii]] += tuples_obs[tuple_key]
            N_obs += tuples_obs[tuple_key]
            

        candidates_field = {}
        for field_id in text_lens_obs[ii]:
            candidates_local = []
            p = tuple_obs_field[field_id] / N_obs * contention_factor
            r = np.sqrt(p * (1-p) / N_obs)
            for cand in text_lens_aux[ii]:
                if text_lens_obs[ii][field_id] == text_lens_aux[ii][cand]:
                    q = tuple_aux_field[cand] / N_aux

                    # if the leakage identifier has a low frequency, use all field values with low frequency as candidates
                    if tuple_obs_field[field_id] < 100 and tuple_aux_field[cand] < 1000:
                        candidates_local += [cand]
                    # otherwise use all field values with close enough frequencies
                    if q > 0.9*p - 2.5*r and q < 1.1*p + 2.5*r:
                        candidates_local += [cand]

            # fall back to the full set of field values if the process above returns an empty set
            if len(candidates_local) == 0:
                for cand in text_lens_aux[ii]:
                    if text_lens_obs[ii][field_id] == text_lens_aux[ii][cand]:
                        candidates_local += [cand]

            candidates_field[field_id] = candidates_local
        candidates += [candidates_field]
    return candidates



'''
# Function to pick a raondom initial solution given candidates
# Randomised amongst all valid assignments built from build_guess_candidates()
# For opLog, we allow more than contention_factor number of field tokens to be assigned to each field value. This is used to avoid not finding an initial solution at the start. 
'''
def get_initial_solution(candidates):
    guess = []
    guess_inv = []

    for candidates_field in candidates:
        guess_field = {}
        guess_field_inv = {}

        for key in candidates_field:
            assignment = random.choice(candidates_field[key])
            count = 0
            global CF_buffer
            while assignment in guess_field_inv and len(guess_field_inv[assignment]) > contention_factor + CF_buffer:
                assignment = random.choice(candidates_field[key])
                count += 1


                # Allow the number of field tokens assigned to each field value to increment by one if we cannot find an initial assignment
                if count == 200:
                    CF_buffer += 1


            guess_field[key] = assignment
            if assignment not in guess_field_inv:
                guess_field_inv[assignment] = []
            guess_field_inv[assignment] += [key]

        guess += [guess_field]
        guess_inv += [guess_field_inv]


    return guess, guess_inv




'''
# Function to run simulated annealing on the individual fields
# Since the old score and new scores are slightly different, we use two functions get_score00() and get_score01() to compute them respectively.
'''
def refine_solution(guess, guess_inv, candidates, candidates_change, tuples_aux, tuples_obs):
    
    for field_idx in range(len(guess)):
        t0 = time.time()

        if len(candidates_change[field_idx]) == 0:
            print(f"Assignments refined for field {field_idx}: %.2f seconds" % (time.time() - t0))
            continue
        
        N_max = max([t[field_idx] for t in tuples_aux if t[field_idx] != None])
        reduced_tuple_aux = [0 for ii in range(N_max+2)]
        for key in tuples_aux:
            reduced_tuple_aux[key[field_idx]+1] += tuples_aux[key]

        N_max = max([t[field_idx] for t in tuples_obs if t[field_idx] != None])
        reduced_tuple_obs = [0 for ii in range(N_max+1)]
        for key in tuples_obs:
            if key[field_idx] != None:
                reduced_tuple_obs[key[field_idx]] += tuples_obs[key]


        temperature = 1000
        for _iter in range(N_iter_refine):
            temperature *= 0.995
            # code to prevent overflow
            if temperature < 0.1**8:
                temperature = 0.1**8
            changes_local = neighbourhood_local(guess, guess_inv, candidates, candidates_change, field_idx)
            score_old = get_score00(guess[field_idx], changes_local, reduced_tuple_aux, reduced_tuple_obs)
            score_new = get_score01(guess[field_idx], changes_local, reduced_tuple_aux, reduced_tuple_obs)

            if score_new > score_old:
                apply_changes_local(guess, guess_inv, changes_local, field_idx)
            elif np.exp((score_new - score_old) / temperature) > random.random():
                apply_changes_local(guess, guess_inv, changes_local, field_idx)

        print(f"Assignments refined for field {field_idx}: %.2f seconds" % (time.time() - t0))


    return guess, guess_inv
        
    


'''
# Neighbourhood function for simulated annealing on individual fields
'''
def neighbourhood_local(guess, guess_inv, candidates, candidates_change, field_idx):
    change_local = {}

    # obtain the key of the field to change
    change_key = random.choice(list(guess[field_idx].keys()))
    attempts = 0
    while attempts < 100 and len(candidates[field_idx][change_key]) == 1:
        change_key = random.choice(candidates_change[field_idx])
        attempts += 1

    # obtain the assignment of the field to change
    assignment_new = random.choice(candidates[field_idx][change_key])
    change_local[change_key] = assignment_new

    # if the assignment is full, swap one of the random elements with the chosen key
    if assignment_new not in guess_inv[field_idx]:
        guess_inv[field_idx][assignment_new] = []
    if len(guess_inv[field_idx][assignment_new]) > contention_factor + CF_buffer:
        change_key_swap = random.choice(guess_inv[field_idx][assignment_new])
        assignment_old = guess[field_idx][change_key]
        change_local[change_key_swap] = assignment_old

    return change_local



'''
# Neighbourhood function for simulated annealing on all fields
'''  
def neighbourhood(guess, guess_inv, candidates, candidates_change, changes_candidate_indices, N_cand_fields_local):
    changes = [{} for ii in range(N_fields)]
    
    changes_indices = set()
    while len(changes_indices) < N_cand_fields_local:
        changes_indices.add(random.choice(changes_candidate_indices))

    for index in changes_indices:
        change_local = {}

        # obtain the key of the field to change
        change_key = random.choice(list(guess[index].keys()))
        attempts = 0
        while attempts < 100 and len(candidates[index][change_key]) == 1:
            change_key = random.choice(candidates_change[index])
            attempts += 1

        # obtain the assignment of the field to change
        assignment_new = random.choice(candidates[index][change_key])
        change_local[change_key] = assignment_new

        # if the assignment is full, swap one of the random elements with the chosen key
        if assignment_new not in guess_inv[index]:
            guess_inv[index][assignment_new] = []
        if len(guess_inv[index][assignment_new]) > contention_factor + CF_buffer:
            change_key_swap = random.choice(guess_inv[index][assignment_new])
            assignment_old = guess[index][change_key]
            change_local[change_key_swap] = assignment_old

        changes[index] = change_local

    return changes, changes_indices



'''
# Function to get reduced/marginal tuple counts given the list of indices we are interested in
'''
def get_reduced_counts(tuple_counts, changes_indices, reduced_tuple_counts_buffer):
    reduced_tuple_counts = {}
    indices = sorted(list(changes_indices))

    if tuple(indices) in reduced_tuple_counts_buffer:
        return reduced_tuple_counts_buffer[tuple(indices)]

    #print(indices)

    for tuple_key in tuple_counts:
        tuple_key_new = []
        for ii in indices:
            tuple_key_new += [tuple_key[ii]]
        if None in tuple_key_new:
            continue
        if tuple(tuple_key_new) not in reduced_tuple_counts:
            reduced_tuple_counts[tuple(tuple_key_new)] = 0
        reduced_tuple_counts[tuple(tuple_key_new)] += tuple_counts[tuple_key]

    reduced_tuple_counts_buffer[tuple(indices)] = reduced_tuple_counts

    return reduced_tuple_counts




'''
# Score function for simulated annealing on a single field (before changes)
'''
def get_score00(guess_local, changes_local, reduced_tuple_counts_aux, reduced_tuple_counts_obs):
    score = 0

    for key in changes_local:
        score += reduced_tuple_counts_obs[key] * np.log(reduced_tuple_counts_aux[guess_local[key]+1])

    return score


'''
# Score function for simulated annealing on a single field (after changes)
'''
def get_score01(guess_local, changes_local, reduced_tuple_counts_aux, reduced_tuple_counts_obs):
    score = 0

    for key in changes_local:
        score += reduced_tuple_counts_obs[key] * np.log(reduced_tuple_counts_aux[changes_local[key]+1])

    return score 


'''
# score function for simulated annealing on all fields (before changes)
'''
def get_score1(guess, reduced_tuple_counts_aux, reduced_tuple_counts_obs, changes_indices):
    score = 0

    for tuple_key in reduced_tuple_counts_obs:
        t0 = time.time()
        tuple_key_change = []
        for ii in range(len(changes_indices)):
            tuple_key_change += [guess[changes_indices[ii]][tuple_key[ii]]]
        tuple_key_change = tuple(tuple_key_change)


        try:
            score += reduced_tuple_counts_obs[tuple_key] * np.log(reduced_tuple_counts_aux[tuple_key_change])
        except:
            continue

    return score        

    
'''
# Score function for simulated annealing on all fields (after changes)
'''
def get_score2(guess, change, reduced_tuple_counts_aux, reduced_tuple_counts_obs, changes_indices):
    score = 0

    for tuple_key in reduced_tuple_counts_obs:
        tuple_key_change = []
        for ii in range(len(changes_indices)):
            if tuple_key[ii] in change[changes_indices[ii]]:
                tuple_key_change += [change[changes_indices[ii]][tuple_key[ii]]]
            else:
                tuple_key_change += [guess[changes_indices[ii]][tuple_key[ii]]]
        tuple_key_change = tuple(tuple_key_change)

        try:
            score += reduced_tuple_counts_obs[tuple_key] * np.log(reduced_tuple_counts_aux[tuple_key_change])
        except:
            continue

    return score  


'''
# Function to apply the change to the current assignment
'''
def apply_changes(guess, guess_inv, changes):
    for ii in range(len(changes)):
        if len(changes[ii]) == 0:
            continue

        for key in changes[ii]:
            value_old = guess[ii][key]
            inv_idx = guess_inv[ii][value_old].index(key)
            del guess_inv[ii][value_old][inv_idx]

            value_new = changes[ii][key]
            guess_inv[ii][value_new] += [key]

            guess[ii][key] = value_new



'''
# Function to apply the change to the current assignment (single field)
'''
def apply_changes_local(guess, guess_inv, change_local, field_idx):
    for key in change_local:
        value_old = guess[field_idx][key]
        inv_idx = guess_inv[field_idx][value_old].index(key)
        del guess_inv[field_idx][value_old][inv_idx]

        value_new = change_local[key]
        guess_inv[field_idx][value_new] += [key]

        guess[field_idx][key] = value_new


'''
# main attack function
'''
def attack(tuples_aux, text_lens_aux, tuples_obs, text_lens_obs):
    candidates = build_guess_candidates(tuples_aux, text_lens_aux, tuples_obs, text_lens_obs)

    # find candidates that can be changed
    candidates_change = []
    for candidates_field in candidates:
        candidates_local = []
        for cand in candidates_field:
            if len(candidates_field[cand]) > 1:
                candidates_local += [cand]
        candidates_change += [candidates_local]

    changes_candidate_indices = []
    for ii in range(len(candidates_change)):
        if len(candidates_change[ii]) > 0:
            changes_candidate_indices += [ii]

    # run simulated annealing on individual fields
    guess, guess_inv = get_initial_solution(candidates)
    guess, guess_inv = refine_solution(guess, guess_inv, candidates, candidates_change, tuples_aux, tuples_obs)
    
    reduced_tuple_counts_aux_buffer = {}
    reduced_tuple_counts_obs_buffer = {}

    time0, time1, time2 = 0,0,0


    # run simulated annealing on all fields
    temperature = 1000
    for _iter in range(N_iter_attack):
        temperature *= 0.995
        # code to prevent overflow
        if temperature < 0.1**8:
            temperature = 0.1**8

        if _iter % (N_iter_attack / 10) == 0:
            print(f"Progress: {_iter} / {N_iter_attack} (%.f%%)" % (_iter/N_iter_attack*100))

        # generating a neighoubourhood
        N_cand_fields_local = random.randrange(1, N_cand_fields_max+1)

        t0 = time.time()
        changes, changes_indices = neighbourhood(guess, guess_inv, candidates, candidates_change, changes_candidate_indices, N_cand_fields_local)
        t1 = time.time()


        # prepare marginal counts for computing the scores later
        reduced_tuple_counts_aux = get_reduced_counts(tuples_aux, changes_indices, reduced_tuple_counts_aux_buffer)
        reduced_tuple_counts_obs = get_reduced_counts(tuples_obs, changes_indices, reduced_tuple_counts_obs_buffer)
        t2 = time.time()

        changes_indices = sorted(list(changes_indices))


        # compute the scores
        score_old = get_score1(guess, reduced_tuple_counts_aux, reduced_tuple_counts_obs, changes_indices)
        score_new = get_score2(guess, changes, reduced_tuple_counts_aux, reduced_tuple_counts_obs, changes_indices)
        t3 = time.time()
        

        # accept the new assignment if it improves the score or it gets lucky
        if score_new > score_old:
            apply_changes(guess, guess_inv, changes)
        elif np.exp((score_new - score_old) / temperature) > random.random():
            apply_changes(guess, guess_inv, changes)

        #print(f"Key len 3: {len(list(tuples_aux.keys())[0])}")

        time0 += t1 - t0
        time1 += t2 - t1
        time2 += t3 - t2

    #print(reduced_tuple_counts_buffer[(0,)])

    print(time0, time1, time2)

    return guess


'''
# function to output the results
'''
def print_results(guess, tuples_obs, file_name_answer, file_name_report):
    answer_file = open(file_name_answer, "r")
    _answer = json.loads(answer_file.readline())
    answer_file.close()

    N_docs = 0
    for tuple_key in tuples_obs:
        N_docs += tuples_obs[tuple_key]

    answer = {}
    for ii in _answer:
        answer[int(ii)] = {}
        for assignments in _answer[ii]:
            answer[int(ii)][assignments[0]] = assignments[1]


    report_file = open(file_name_report, "w")
    for field_idx in range(len(guess)):
        N_correct = 0
        N_correct_doc = 0
        N_total = 0


        reduced_tuples_obs = {}
        for tuple_key in tuples_obs:
            if tuple_key[field_idx] not in reduced_tuples_obs:
                reduced_tuples_obs[tuple_key[field_idx]] = 0
            reduced_tuples_obs[tuple_key[field_idx]] += tuples_obs[tuple_key]

        for key in guess[field_idx]:
            if guess[field_idx][key] in answer[field_idx] and key in answer[field_idx][guess[field_idx][key]]:
                N_correct += 1
                N_correct_doc += reduced_tuples_obs[key]
                
            N_total += 1

        print(f"Field {field_idx}, key: {N_correct}/{N_total} (%.2f%%)" % (N_correct/N_total*100))
        print(f"Field {field_idx}, doc: {N_correct_doc}/{N_docs} (%.2f%%)" % (N_correct_doc/N_docs*100))

        report_file.write(f"{N_correct},{N_total},{N_correct_doc},{N_docs}\n")

    report_file.close()



if __name__ == "__main__":
    parser = argparse.ArgumentParser("Attack")
    parser.add_argument("--oplog", action='store_true', help="with leakage extraction/simulated from oplog")
    parser.add_argument("--uniform", dest='uniform', metavar='u', type=int, help="queries from uniform distribution")
    parser.add_argument("--zipf", dest='zipf', metavar='z', type=int, help="queries from Zipf distributions")
    parser.add_argument("--exp", dest='expnum', metavar='e', type=int, help="experiment number")
    parser.add_argument("--cf", dest='cf', metavar='c', type=int, help="contention factor, counting from 1; default value for query leakage = 1, compaction leakage = 5")
    parser.add_argument("--fast", action='store_true', help="use fewer iterations for fast completion")
    parser.add_argument("--without", action='store_true', help="without length leakage")

    parser.add_argument("--aux", type=str, default="", help="auxiliary information file")
    parser.add_argument("--docmatrix", type=str, default="", help="leakage profile file")
    parser.add_argument("--answer", type=str, default="", help="answer map file")
    parser.add_argument("--report", type=str, default="", help="report file")    
    
    # pre-processing
    if len(sys.argv) == 6 and sys.argv[1] == "--manual":
        file_name_aux = sys.argv[2]
        file_name_leakage = sys.argv[3]
        file_name_answer = sys.argv[4]
        file_name_report = sys.argv[5]
        sys.argv = [sys.argv[0]] + ["--aux", f"{file_name_aux}", "--docmatrix", f"{file_name_leakage}",
                                    "--answer", f"file_name_answer", "--report", f"file_name_report"]

    args = parser.parse_args()

    leakage_type, query_type = "compaction", ""
    N_queries = 0
    
    if args.oplog != None:
        if args.oplog:
            contention_factor = 5 # default value in QE
            leakage_type = "compaction"

    if args.uniform != None:
        contention_factor = 1
        leakage_type = "query"
        query_type = "uniform"
        N_queries = args.uniform

    if args.zipf != None:
        contention_factor = 1
        leakage_type = "query"
        query_type = "zipf"
        N_queries = args.zipf
        

    if args.cf != None:
        contention_factor = args.cf
        

    if args.fast != None and args.fast:
        N_iter_refine = 10**6
        N_iter_attack = 10**3

    print(f"Experiment type = {leakage_type}", end="")
    if leakage_type == "query":
        print(f"({query_type})")
    else:
        print("")
        
    if args.expnum == None:
        args.expnum = 0

    print(f"Experiment number = {args.expnum}")
        
    # index inputs for job submission
    if args.without != None and args.without:
        file_name_aux = f"../aux_info/acs/2012/aux_info_3113030_6.json"
        file_name_leakage = f"../doc_matrix/{leakage_type}/acs/2013/exp_{args.expnum}_3132795_6.json.gz"
        file_name_answer = f"../answer/{leakage_type}/acs/2013/exp_{args.expnum}_answer_3132795_6.json"
        file_name_report = f"../report_{leakage_type}_{args.expnum}"
    else:
        file_name_aux = f"../aux_info/acs/2012/aux_info_len_leakage_3113030_6.json"
        file_name_leakage = f"../doc_matrix/{leakage_type}/acs/2013/exp_{args.expnum}_3132795_with_len_6.json.gz"
        file_name_answer = f"../answer/{leakage_type}/acs/2013/exp_{args.expnum}_answer_with_len_3132795_6.json"
        file_name_report = f"../report_{leakage_type}_with_len_{args.expnum}"

    time_start = time.time()

    guess = None
    if leakage_type == "compaction":
        tuples_aux, text_lens_aux, tuples_obs, text_lens_obs = read_files(file_name_aux, file_name_leakage)
        guess = attack(tuples_aux, text_lens_aux, tuples_obs, text_lens_obs)

    elif leakage_type == "query" and query_type == "uniform":
        tuples_aux, text_lens_aux, tuples_raw, text_lens_raw = read_files(file_name_aux, file_name_leakage)
        tuples_obs, text_lens_obs = generate_observed_data_uniform(tuples_raw, text_lens_raw, N_queries)
        guess = attack(tuples_aux, text_lens_aux, tuples_obs, text_lens_obs)

    elif leakage_type == "query" and query_type == "zipf":
        tuples_aux, text_lens_aux, tuples_raw, text_lens_raw = read_files(file_name_aux, file_name_leakage)
        tuples_obs, text_lens_obs = generate_observed_data_zipf(tuples_raw, text_lens_raw, N_queries)
        guess = attack(tuples_aux, text_lens_aux, tuples_obs, text_lens_obs)

    else:
        print("Please provide valid arguments!")
        exit(1)
        

    time_end = time.time()
    print(f"Time elapsed: %.2f seconds" % (time_end-time_start))

    print_results(guess, tuples_obs, file_name_answer, file_name_report)
