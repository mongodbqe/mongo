# Paper
* Title: Security Analysis of MongoDB Queryable Encryption (Usenix Security 23')
* Authors: Zichen Gui, Kenneth G. Paterson, and Tianxin Tang
* Paper: [link](https://www.usenix.org/conference/usenixsecurity23/presentation/gui) (to be available after 11th August 2023)

# Paper abstract
In June 2022, MongoDB released Queryable Encryption (QE), an extension of their flagship database product, enabling keyword searches to be performed over encrypted data. This is the first integration of such searchable encryption technology into a widely-used database system. We provide an independent security analysis of QE. We show that certain logs, fundamental to the operation of QE and accessible to a real-world snapshot adversary, contain statistical information about the queries and data. This information can be extracted and exploited by our new inference attacks to recover both the queries and data, assuming adversarial access to an auxiliary dataset with a similar distribution to the original data.
Our analysis highlights the challenges of integrating searchable encryption technology into modern, complex database systems. In particular, our attacks stem from the interplay between QE and MongoDB’s existing logging system. They show how such interactions can compromise query and data privacy.

# Artifact abstract
This artifact is provided alongside our paper. We describe the two phases of our attack on MongoDB QE. The first phase is *leakage extraction* (Section 4), followed by the second phase *inference attacks* (Section 7). Due to the complications with QE, leakage extraction is not feasible on a large-scale database, and we had to work with simulated leakage in our experiments (see Appendix B). Therefore, we provide two procedures for leakage extraction, one for real leakage (E1) and the other for simulated leakage (E2). The procedures for inference attacks exploiting leakage from queryLog and opLog can be found in E3 and E4, respectively.

## MongoDB version
This artifact works for MongoDB version 6.X.X. We have tested this artifact up to MongoDB version 6.2.1-rc1.

# Repo layout
## Directory tree

<details>
<summary>Directory tree (collapsable)</summary>

```
.
├── E1_sample_output.txt
├── E2_sample_output.txt
├── E3_sample_output.txt
├── E4_sample_output.txt
├── LICENSE
├── README.md
├── acs_data
│   ├── 2012_person_records
│   ├── 2013_person_records
│   └── ACSPUMS2009_2013CodeLists
│       ├── COW.csv
│       ├── OCC Crosswalk 2010-2012.csv
│       ├── OCC recode 2010, 2011 to 2012.csv
│       ├── OCCP02 & SOCP00.csv
│       ├── OCCP10 & SOCP10.csv
│       ├── OCCP12 & SOCP12.csv
│       ├── POWPUMA-2009-2013-lookup.csv
│       ├── Place of birth.csv
│       ├── Place of work.csv
│       ├── RAC1P.csv
│       ├── RAC2P05.csv
│       ├── RAC2P12.csv
│       ├── RAC3P05.csv
│       ├── RAC3P12.csv
│       └── ST.csv
├── answer
├── aux_info
│   └── acs
│       └── 2012
│           ├── aux_info_3113030_6.json
│           └── aux_info_len_leakage_3113030_6.json
├── dataset
├── doc_matrix
├── fields
├── log
├── oplog
├── snapshot
├── src
│   ├── ACSDataExporter.py
│   ├── DataExporter.py
│   ├── Parser.py
│   ├── check_config.py
│   ├── client.py
│   ├── correctness.py
│   ├── export_acs_data_sample.py
│   ├── export_acs_data_simulated.py
│   ├── main.py
│   ├── make_data_key.py
│   ├── master-key.txt
│   ├── parameters.py
│   ├── parse_oplog.py
│   ├── parse_query_log.py
│   ├── requirements.txt
│   ├── run_batched_commands.sh
│   └── util.py
└── src_attack
    └── attack.py
```
</details>

## File descriptions
### Dataset and auxiliary information
* 📁 `acs_data` contains 
  * 📁 `2012_person_records` to store raw `ss12pusa.csv` and `ss12pusb.csv`
  * 📁 `2013_person_records` to store raw `ss13pusa.csv` and `ss13pusb.csv`
  * 📁 `ACSPUMS2009_2013CodeLists` containing codebook information for decoding the raw data values
* 📁 `dataset` is used to store processed dataset before inserting with MongoDB QE. This directory stores processed dataset from `src/export_acs_data_sample.py`.
  * 📁 `fields` is used to store intermediate auxiliary information such as the (sorted) unique field values, extracted from the dataset. This information is later used for leakage simulation, and to compute auxiliary information used as part of the inference attacks.
* `aux_info/acs/2012/aux_info_len_leakage_3113030_6.json` is auxiliary information extracted for 2012 acs dataset, using `src/export_acs_data_sample.py` with `data_year = 2012`. This is provided for convenience and is used as auxiliary information to recover 2013 dataset in E3 and E4, respectively. `3113030` is the number of records.
  <details>
  <summary> Information about this auxiliary file (collapsable)</summary>

    ```
    # tuple frequency, each tuple contains 6 identifiers, followed with the count
    
    "[0, 39, 54, 39, 0, 18]": 2,
    "[0, 39, 31, -1, 1, 43]": 1

    # length leakage, "0" is field 0, followed by the identifier: length in bytes
     {"0": {"-1": 215, "0": 226, "1": 246,...}}
    ```
   </details>
    
  
### Source
* 📁 `src` contains Python package dependencies, code for processing raw datasets, leakage extraction, and leakage simulation.
  * Python package dependencies
    * `requirements.txt` lists all Python package dependencies.
  * Check configuration
    * `check_config.py` is a basic test for library and dataset configuration.
  * Dataset pre-processing
    * `export_acs_data_sample.py` takes as input raw dataset and outputs processed dataset in JSON format in `dataset` directory, ready for insertion using MongoDB QE. This module also exports the auxiliary information of the dataset.
    * `DataExporter.py` specifies `DataExporter` class with basic methods.
    * `ACSDataExporter.py` extends `DataExporter.py` for ACS dataset.
  * Perform QE operations, collect logs, and extract leakage
    * This task is achieved by `main.py`.
    * `Parser.py` provides a base class `Parser`. `parse_oplog.py` implements `OpLogParser` extending `Parser`;`parse_query_log.py` implements `LogParser` extending `Parser`.
    * `client.py` is MongoDB QE client-side code, provides basic methods such as database initialization, document insertion and find.
    * `correctness.py` checks the correctness for leakage profiles extracted from the raw querylog and oplog.
  * Leakage simulation
    * `export_acs_data_simulated.py`: leakage simulation for querylog, oplog, with and without length leakage, followed by correctness check.
  * `make_data_key.py` is used for initializing a QE-encrypted document collection. 
  * `master-key.txt` is a sample 96-byte master secret key provided for QE encryption, can also be generated new using OpenSSL.
  * `parameters.py` specifies all paths and parameters for this `src` directory.
  * `parse_oplog.py` parses raw querylog resulting from insertion and compaction operations, and extracts a leakage profile (doc_matrix).
  * `parse_query_log.py` parses raw querylog resulting from insertion and find operations, and extracts a leakage profile (doc_matrix).
  * `main.py` supports functionality of automatic logs collection, leakage extraction, and correctness check.
  * Extra
    *  `run_batched_commands.sh` is optional for generating simulate leakage profiles in batches using multicores.
    * `util.py` contains utility functions.
* `src_attack/attack.py` is the code for the inference attacks using leakage profiles extracted/simulated from queryLog or opLog, with auxiliary information provided (ACS 2012).
### Output
* 📁 `log` is used to store the raw queryLog file when running E1.
* 📁 `oplog` is used to store the raw opLog file when running E1.
* Example output files for the experiments
  * `E1_sample_output.txt` for E1
  * `E2_sample_output.txt` for E2
  * `E3_sample_output.txt` for E3
  * `E4_sample_output.txt` for E4
* 📁 `doc_matrix` is used to store leakage profiles extracted from queryLog or opLog for the encrypted documents. These outputs are from the leakage extraction (E1) or leakage simulation (E2).

  <details>
    <summary> Representation of the doc_matrix file (collapsable)</summary>
   
    ```
     {
      "i": 0,
      "0": [58, 218],
      "1": [ 0, 217],
      "2": [11, 218],
      "3": [-1, 215],
      "4": [-1, 215],
      "5": [-1, 215]
    }
    ```
    </details>
   This JSON snippet is described by the JSON schema below. We omit the descriptions for field "1" to field "5", as they follow the same format of field 0.
  <details>
    <summary> JSON schema of the doc_matrix file (collapsable)</summary>
   
    ```
    {
      "i": {
          "key description": "abbreviation of index; i is a fixed key for all encrypted documents"
          "value description": "the index of the encrypted document",
          "value type": "integer"
      },
      "0": {
          "key description": "the index of the field; 0 means the first field",
          "value description": " the value takes the shape of [x, y]; x is the (token) identifier, and y is the length of the encrypted field value. We use x = -1 to denote the empty field value; y = 215 corresponds to the length of encrypted field value that has a plaintext of length 0.",
          "value type": "array of two integers [x, y]"
      }
    } 
    ```
  </details>
      
  
* 📁 `snapshot` is used to store a snapshot of encrypted document collection encrypted by QE.
  <details>
  <summary> Representation of the snapshot file (collapsable)</summary>

    ```
    {
      "_id": {
          "$oid": "64a850645af5231967c40aaa"
      },
      "RAC3P": {
          "$binary": {
              "base64": "B3zOdnnxuETerV2Gmjm+5HkCXNS/p4xC9y5nBgPjbPVNlGuLalM3C264Pexp3TuRLy8GtrnYH3nNDnzQjwRW706XfjC9qIZHlT5YHXjijTu/XEnH9WYdP+a1FKweBh8QrsloGQajN0mxG7bAq8rJYN6goq+zA3pz3SdAIHiJp5K4lElmnG+iWy16BQjPJyoS8kskutTrZfZx1C+os+CV02VclLBALWbYWrxEq0LsxFr60lSwAtcSYYth0ZWQfN9hb8TbLIM5Lk3s3tYVb7s6IZscHscY2tO5G0E=",
              "subType": "06"
          }
      }
    }
    ```
   
  </details>
  This JSON snippet is described by the JSON schema below. The description of field "RAC3P" applies to all the other fields in the ACS dataset.
  <details>
  <summary> JSON schema of the snapshot file (collapsable)</summary>

  ```
  {
      "_id": {
          "$oid": {
              "value description": "MongoDB ObjectId of the encrypted document",
              "value type": "string"
          }
      },
      "RAC3P": {
          "key description": "field name used in the ACS dataset",
          "$binary": {
              {
                  "base64": {
                      "value description": "ciphertext of the encrypted field value",
                      "value type": "string"
                  },
                  "subType": {
                      "value description": "internal BSON subtype for field RAC3P.$binary.base64; 06 indicates that this is encrypted field; 06 is used for all encrypted fields in the ACS dataset.",
                      "value type": "string"
                  }
              },
          }
      }
  }
  ```
  </details>

* 📁 `answer` is used to store the answer for inference attack, namely the (random and secret) mapping between identifiers and the unique field values. This corresponds to the recovery target, and the recovery rate is computed based on the number of unqiue field values recovered per field, and the number of documents have been recovered per field.
  <details>
  <summary> Representation of the answer file (collapsable)</summary>

    ```
    {
      "0": [
          [-1, [247, 90, 197, 383, 470]],

      ]
    }
    ```
   
  </details>
  This JSON snippet is described by the JSON schema below. We only show the description for field 0 (corresponding to "RAC3P" in the ACS dataset). For field 0, we only show the first (secret) mapping between the plaintext field value identifier and the corresponding token identifiers in the encrypted documents.
  <details>
  <summary> JSON schema of the answer file (collapsable)</summary>

    ```
    {
        "0": {
            "key description": "the index of the field; 0 means the first field",
            "value description": "an array A of arrays",
            "value type": "array",
            "A[0]": {
                "key description": "0 means the first element of array A; the same format applies to all the other elements of A.",
                "value description": "an array that takes shape [x, y]; x is an identifier for the plaintext field value (sorted), y is the corresponding token identifier in the encrypted documents",
                "value type": "array"
            }
        }
    }
    ```
   
  </details>


# Requirements
* Leakage simulation and inference attacks require resources of 10 GB RAM, 16 GB disk, and ∼ 3.3 GHz CPU on a single core (is recommended).
* For small-scale leakage extraction (e.g., 3K - 10K records), the same specification is adequate. However, full-scale leakage extraction involving building a database containing 3M records requires 50 GB RAM and 600 GB disk.
* The instructions provided here only work for Linux/Unix systems. Ubuntu 22.04 is used for evaluation.

# Software dependencies
## General
**The links provided above direct you to the packages of the latest version only. To reproduce our results with the specific versions of the packages we used in our artifact, please refer to the following example. The instruction works specifically for Ubuntu 22.04. We provide further instruction for other operating systems later.**

* MongoDB Enterprise libraries: https://www.mongodb.com/download-center/enterprise/releases
	- [ ] `mongod`: from `Archive` (6.0.7 tested, can edit the version number in the url provided in the website if only newer version show up. We have tested the evaluation up to version 6.2.1-rc1.)
	- [ ] `mongo_crypt_v1.so` : from `crypt_shared` (6.0.7 tested)
* Mongocrypt library: https://www.mongodb.com/docs/manual/core/csfle/reference/libmongocrypt
  - [ ] `libmongocrypt` (1.7.4 tested)
 **Notice**: for Debian/Ubuntu installation, you need to use
 `sudo apt-get install -y libmongocrypt-dev`; the provided `libmongocrypt` in the manual cannot be found in the source.
* MongoDB database tools: https://www.mongodb.com/docs/database-tools/installation/installation
	- [ ] `mongoexport` (100.7.3 tested)
* MongoDB shell: https://www.mongodb.com/try/download/shell
	- [ ] `mongosh` (1.10.1 tested)

## Example for Ubuntu 22.04
- [ ] `mongod Archive` (6.0.7): https://downloads.mongodb.com/linux/mongodb-linux-x86_64-enterprise-ubuntu2204-6.0.7.tgz
- [ ] `crypt_shared` (6.0.7): https://downloads.mongodb.com/linux/mongo_crypt_shared_v1-linux-x86_64-enterprise-ubuntu2204-6.0.7.tgz
- [ ] `libmongocrypt` (1.7.4) Please use the following commands.
  
  ```
  sudo sh -c 'curl -s --location https://www.mongodb.org/static/pgp/libmongocrypt.asc | gpg --dearmor >/etc/apt/trusted.gpg.d/libmongocrypt.gpg'

  echo "deb https://libmongocrypt.s3.amazonaws.com/apt/ubuntu jammy/libmongocrypt/1.7 universe" | sudo tee /etc/apt/sources.list.d/libmongocrypt.list

  sudo apt-get update

  sudo apt-get install -y libmongocrypt-dev=1.7.4-0
  ```

- [ ] `mongoexport` (100.7.3): https://fastdl.mongodb.org/tools/db/mongodb-database-tools-ubuntu2204-x86_64-100.7.3.tgz
- [ ] `mongosh` (1.10.1): https://downloads.mongodb.com/compass/mongodb-mongosh_1.10.1_amd64.deb

Similarly, for other operating systems/architectures, you can obtain the packages of specific versions by modifying the operating system/architecture of the links above. See the download links in "General" for examples.
# Benchmarks
We use ACS 2012 as *auxiliary data* and ACS 2013 as the *recovery target* in our experiments.
- [ ] [ACS 2012](https://www2.census.gov/programs-surveys/acs/data/pums/2012/1-Year/csv_pus.zip)
- [ ] [ACS 2013](https://www2.census.gov/programs-surveys/acs/data/pums/2013/1-Year/csv_pus.zip)

# Set-up
## Installation
- [ ] Download `csv_pus.zip` for ACS 2012, ACS 2013, repectively listed in Benchmarks. Unzip the files and get `ss12pusa.csv`, `ss12pusb.csv`, `ss13pusa.csv`, and `ss13pusb.csv`.
- [ ] Place `ss12pusa.csv` and `ss12pusb.csv` in `mongo/acs_data/2012_person_records`; place `ss13pusa.csv` and `ss13pusb.csv` in `mongo/acs_data/2013_person_records`.
- [ ] Work in `mongo/src/` and create a Python virtual environment and install the required packages:
	- [ ] `python3 -m pip install --user virtualenv`
	- [ ] `python3 -m venv env`
	- [ ] `source env/bin/activate`
	- [ ] `pip3 install -r requirements.txt`
	- [ ] (optional) `deactivate`
- [ ] Install `mongod`, `crypt_shared`, `libmongocrypt`, `mongoexport`, `mongosh`; urls are listed in Software dependencies.
- [ ] Configure the above library dependencies in `mongo/src/parameters.py`, from line 53 to line 56.
- [ ] Configure the default MongoDB database path at line 57 in `mongo/src/parameters.py`. Please specify a directory that does not require root access.

## Basic test
- [ ] Working in `mongo/src`, run `python3 check_config.py` to check whether the installation and configuration are complete.
- Given the nature of this artifact, we cannot think of a more effective way to provide a self-contained, minimal, working example that demonstrates its functionality. We recommend following the evaluation workflow to understand what this artifact offers.

# Evaluation workflow

## Major claims

* **(C1)**: Real leakage about the data encrypted by QE can be extracted from opLog alone or from queryLog and encrypted document collection (Appendix B).
* **(C2)**: Leakage simulations for opLog and queryLog pass the correctness check, respectively, matching the real leakage (Appendix B).
* **(C3)**: The inference attack exploiting simulated query leakage (under uniform and Zipf distributions, with 100, 300, 500 queries per field) from queryLog achieve reasonable recovery rates (Section 7.2).
* **(C4)**: The inference attack exploiting simulated compaction leakage from opLog achieve reasonable recovery rates (Section 7.2).

## Experiments
The evaluation workflow provided below only creates one instance of our attack. The experimental results shown in our paper are produced by multiple instances of this evaluation workflow. **Note that E3 and E4 can be run concurrently to reduce the waiting time.**

### E1. Leakage extraction
* **Preparation**: Working in `mongo/src`, run `python3 export_acs_data_sample.py` to generate auxiliary information.
* **Execution**: `python3 main.py` to collect queryLog and opLog, extract leakage, and check its correctness for the sample dataset.
* **Results**: Real leakage about the data can be extracted from logs successfully.

* **Justification**: 
  * Operations performed in `export_acs_data_sample.py`
    1. This extract a random subset of data JSON of 3K records from 2013 ACS dataset (default).
    2. `aux_info`, auxiliary information of this 3K records, used for correctness check.
   
  * Operations performed in `main.py`
    1. `main.py` calls `client.py` to initialize a QE encrypted document collection, then perform document insertion (with 6 encrypted fields), and then perform query on the field values; followed by a compaction procedure.
    2. Move and export the raw querylog, snapshot of encrypted documents, and oplog for leakage extraction.
    3. `main.py` calls `parse_query_log.py` and `parse_oplog.py` to extract leakage profiles (doc_matrix) from querylog and oplog, respectively.
    4. `main.py` calls `correctness.py` to check the correctness of extracted leakage profiles.
  * Verifying **C1**
    * This claim matches the leakage profile extracted from the oplog and querylog. This is demonstrated by the doc_matrix files output by,  `parse_oplog.py` and `parse_query_log.py`. 
   
    * To show the correctness, we implemented the module `src/correctness.py`. Also please refer to (Appendix B) in our paper.
    
      1. Correctness check of leakage extracted from querylog
          - We implement the check by using the frequency information (aux_info) on the tuple values and field values of the plaintext (not used in the attack)
          - Compute the tuple and unique field frequencies from the doc_matrix. 
          - Check whether the tuple frequency and also unique field frequency are the same as in the plaintext. 
          - Query leakage with length is ignored as the frequency information stays the same, and only the length information is added.

      2. Correctness check of leakage extracted from oplog
         - We check the number of tokens observed (extracted) for each field with the possible number (from plaintext).
         - With a high probablity inserting large number of records (> 3 million), the number of tokens observed exactly matches the number: for each field name, number of (all unique field values + empty string (if present)) * (max_contention factor + 1). Note that, this check suffers a small failure probability due to randomness.
         - With smaller number of data points, we ensure that it is smaller than the number above.

### E2. Leakage simulation
Leakage simulation. 5 human-minutes + 40 compute-minutes (depending on the number of experiments) + 16 GB disk:

* **Preparation**: Make sure Set-up is complete. Work in `mongo/src`.
* **Execution**: `python3 export_acs_data_simulated.py --start=0 --end=1` generates one instance of simulated leakage, denoted as `exp_0`.
* **Results**: Leakage is simulated for opLog and queryLog correctly.
* **Justification**:
  * Operations performed in `export_acs_data_simulated.py`
    1. Generate simulated leakage `doc_matrix` for querylog and oplog, with and without length leakage.
    2. The `aux_info` for ACS 2023 is generated for correctness check. This stores frequency information of each tuple. Each tuple contain number_encrypted_fields identifiers. Each identifier uniquely maps to a unique field value under the same field name. The identifier used indicates the order of the (sorted) field values (generated earlier) under some field name. 
  * Verifying **C2**:
    * Leakage is simulated using the algorithm in Figure 7 of Appendix B.
    * Passed the correctness check (using the same module on the module `correctness.py`) as in E1, called at the end of `export_acs_data_simulated.py`; also indicated in the doc_matrix ouput.  Due to the randomness introduced by the contention factor, the correctenss of leakage extracted from oplog is further evaluated emperically using the inference attack. For more details see Appendix B. 


### E3. Inference attack with queryLog
Inference attack with leakage from queryLog. 5 human-minutes + 3 compute-hours (or 2 minutes if using `--fast` option) + 16 GB disk:
* **Preparation**: 
  * Work in `mongo/src_attack`. 
  * (Optional) Core attack parameters such as the number of iterations can be set from line 12 to line 29 of `mongo/src_attack/attack.py`.
* **Execution**: If using simulated leakage from queryLog generated in E2, then run: `python3 attack.py`. 
  * Command 1: `python3 attack.py --uniform n` with `n` in `{100, 300, 500}`(3 compute-hours)
  * Command 2: `python3 attack.py --zipf n` with `n` in `{100, 300, 500}` (3 compute-hours)
  * Optional argument: `--fast` with fewer iterations (2 compute-minutes)
  * Optional argument: `--without` without length leakage 
  * Please use `--help` for more options.
* **Results**: The inference attack using leakage from queryLog achieves a reasonable recovery rate (see **C3**).
* **Justification**: 
  * We provide the justification for our data reconstruction attack against  uniformly distributed queries. A similar justification also applies to our attacks against Zipf distributed queries.
  * Operations performed in `python3 attack.py --uniform 300`
    1. It takes as input the doc_matrix extracted from `querylog`, `mongo/doc_matrix/query/acs/2013/exp_0_3132795_6_with_len.json.gz` (default) and auxiliary information `mongo/aux_info/acs/2012/aux_info_len_leakage_3113030_6.json`.
    2. It extracts partial simulated query leakage (doc_matrix), simulated from `n` queries per field from the uniform distribution. This procedure can be found in `generate_observed_data_uniform` from line 79 to line 114 in `attack.py`.
    3. Then the attack procedure utilizes `refine_solution` (line 263 to line 302) implementing simulated anealing (Figure 9 in our paper).
    4. Output `query_report`, and also standard printout specifying the recovery rates for each encrypted fields and the elapsed time.
    5. <details> <summary> sample output --uniform 300 (collapsable)</summary>
  
        ```
          (env) $ python3 attack.py --uniform 300
          ...
          Field 0, key: 78/97 (80.41%)
          Field 0, doc: 3125484/3132795 (99.77%)
          Field 1, key: 46/51 (90.20%)
          Field 1, doc: 2957231/3132795 (94.40%)
          Field 2, key: 100/166 (60.24%)
          Field 2, doc: 2151383/3132795 (68.67%)
          Field 3, key: 54/60 (90.00%)
          Field 3, doc: 3040278/3132795 (97.05%)
          Field 4, key: 10/10 (100.00%)
          Field 4, doc: 3132795/3132795 (100.00%)
          Field 5, key: 170/222 (76.58%)
          Field 5, doc: 729270/3132795 (23.28%)
        ```
        </details> 
        We provide a sample output of running the inference attack on the simulated leakage for 300 uniformly distributed queries per field.
        The output shows two types of recovery rates. Using Field 0 (out of 6 fields) as an example. Field 0 records RAC3P, the detailed racial profile.
       
       ```
        Field 0, key: 78/97 (88.41%)
        Field 0, doc: 3125484/3132795 (99.77%)
       ```
        The first row of the sample output reports that out of the 97 unique field values that have been queried on field 0, 78 of them (88.41%) have been successfully recovered. The relatively high recovery rate is reflected in Figure 4 (third column of the top diagram) in our paper.

        The second row of the sample output reports that out of the 3132795 documents touched by queries on any field, 3125484 documents (99.77%) on field 0 have been successfully recovered. The high recovery rate is reflected in Figure 4 (fourth column of the top diagram) in our paper.

       
    6. Additonal sample output:
        <details> 
        <summary>  --uniform 300 --without (collapsable)</summary>

        ```
          (env) $ python3 attack.py --uniform 300 --without  
          ...
          Field 0, key: 19/97 (19.59%)
          Field 0, doc: 3063069/3132795 (97.77%)
          Field 1, key: 23/51 (45.10%)
          Field 1, doc: 1937084/3132795 (61.83%)
          Field 2, key: 16/158 (10.13%)
          Field 2, doc: 966498/3132795 (30.85%)
          Field 3, key: 21/60 (35.00%)
          Field 3, doc: 2448281/3132795 (78.15%)
          Field 4, key: 10/10 (100.00%)
          Field 4, doc: 3132795/3132795 (100.00%)
          Field 5, key: 16/224 (7.14%)
          Field 5, doc: 1591518/3132795 (50.80%)
       ``` 
       </details>
          <details> 
        <summary>  --zipf 300 (collapsable)</summary>

        ```
          (env) $ python3 attack.py --zipf 300 
          ...
          Field 0, key: 53/68 (77.94%)
          Field 0, doc: 613535/3132795 (19.58%)
          Field 1, key: 36/44 (81.82%)
          Field 1, doc: 2437129/3132795 (77.79%)
          Field 2, key: 65/96 (67.71%)
          Field 2, doc: 864376/3132795 (27.59%)
          Field 3, key: 44/55 (80.00%)
          Field 3, doc: 2949371/3132795 (94.15%)
          Field 4, key: 10/10 (100.00%)
          Field 4, doc: 3132795/3132795 (100.00%)
          Field 5, key: 95/122 (77.87%)
          Field 5, doc: 1653917/3132795 (52.79%)

       ``` 
       </details>
        <details> 
        <summary>  --zipf 300 --without (collapsable)</summary>

        ```
          (env) $ python3 attack.py --zipf 300 --without  
          ...
          Field 0, key: 13/74 (17.57%)
          Field 0, doc: 187424/3132795 (5.98%)
          Field 1, key: 19/46 (41.30%)
          Field 1, doc: 1650180/3132795 (52.67%)
          Field 2, key: 13/97 (13.40%)
          Field 2, doc: 640617/3132795 (20.45%)
          Field 3, key: 20/51 (39.22%)
          Field 3, doc: 2377939/3132795 (75.90%)
          Field 4, key: 9/10 (90.00%)
          Field 4, doc: 3070483/3132795 (98.01%)
          Field 5, key: 8/127 (6.30%)
          Field 5, doc: 1333673/3132795 (42.57%)
       ``` 
</details>
   
  * Verifying **C3**:
    * The above justification allows us to validate C3: we achieve a reasonable recovery rate across all fields, even with a limited number of queries derived from either the uniform or Zipf distributions. 

### E4. Inference attack with opLog
Inference attack with leakage from opLog. 5 human-minutes + 14 compute-hour (or 20 minutes if using `--fast` option) + 16 GB disk:

* **Preparation**: Same as in **E3**.
* **Execution**: 
  * Command: `python3 attack.py --oplog` with default parameters (14 compute-hour)
  * Optional argument: `--fast` with fewer iterations (20 compute-minutes)
  * Optional argument: `--without` without length leakage 
  * Please use `--help` for more options.
* **Results**: The inference attack using leakage from opLog achieves a reasonable recovery rate (see **C4**).
* **Justification**: 
  * Opertions performed in `attack.py --oplog`
    1. It takes as input the doc_matrix extracted from `oplog`, `mongo/doc_matrix/compaction/acs/2013/exp_0_3132795_6_with_len.json.gz` (default) and auxiliary information `mongo/aux_info/acs/2012/aux_info_len_leakage_3113030_6.json`
    2. It then utilizes the same attack subroutines as in E3. The main difference lies in the fact that the compaction leakage from oplog does not require any find queries. In addition, the maximum contention factor is set to 5 in our code, counting from 1. Note that this is different from using `cf_max = 4`, counting from 0 in our paper. As compaction procedure cannot aggregate field values inserted with different contention factors the way query leakage can.
    2. Output `compaction_report`, and also standard printout specifying the recovery rates for each encrypted fields and the elapsed time.
    3. <details> 
        <summary>  Sample output --oplog (collapsable)</summary>

        ```
          (env) $ python3 attack.py --oplog 
          ...
          Field 0, key: 384/500 (76.80%)
          Field 0, doc: 3126484/3132795 (99.80%)
          Field 1, key: 239/255 (93.73%)
          Field 1, doc: 3017411/3132795 (96.32%)
          Field 2, key: 590/1075 (54.88%)
          Field 2, doc: 2730979/3132795 (87.17%)
          Field 3, key: 268/300 (89.33%)
          Field 3, doc: 3066105/3132795 (97.87%)
          Field 4, key: 50/50 (100.00%)
          Field 4, doc: 3132795/3132795 (100.00%)
          Field 5, key: 1523/2400 (63.46%)
          Field 5, doc: 2917781/3132795 (93.14%)
       
       ``` 
       </details>

       ```
        Field 0, key: 384/500 (76.80%)
        Field 0, doc: 3126484/3132795 (99.80%)
       ```
        The first row of the sample output reports that out of the 500 unique field values (with contention factor) on field 0, 384 of them (76.80%) have been successfully recovered. The relatively high recovery rate is reflected in Figure 5 (third column) in our paper.

        The second row of the sample output reports that out of the 3132795 documents, 3125484 documents (99.80%) on field 0 have been successfully recovered. The high recovery rate is reflected in Figure 5 (fourth column) in our paper.
    
    4. <details> 
        <summary> Sample output --oplog --without (collapsable)</summary>
        
        ```
          (env) $ python3 attack.py --oplog --without  
          ...
          Field 0, key: 83/500 (16.60%)
          Field 0, doc: 2997379/3132795 (95.68%)
          Field 1, key: 94/255 (36.86%)
          Field 1, doc: 1618327/3132795 (51.66%)
          Field 2, key: 114/1075 (10.60%)
          Field 2, doc: 1422432/3132795 (45.40%)
          Field 3, key: 93/300 (31.00%)
          Field 3, doc: 2423394/3132795 (77.36%)
          Field 4, key: 43/50 (86.00%)
          Field 4, doc: 2995293/3132795 (95.61%)
          Field 5, key: 64/2400 (2.67%)
          Field 5, doc: 1519788/3132795 (48.51%)
       ``` 
       </details>  
  * Verifying **C4**:
    * The above justification allows us to validate C4: we get high recovery rate on fields RAC3P (race) and COW (class of work), and reasonable recovery rates for the rest of the fields.

# Extra commands
## Generate simulated leakage in batches
* Generate simulated leakage using `python3 export_acs_data_simulated.py --start=0 --end=1` for `exp_0`
* The argument `--start`, `--end` are used to indicate the start and the end (exclusive) experiment number.
* The argument `--limit` can be used for limiting the number of records included in the leakage simulation.
* Please refer to `run_batched_commands.sh` for a usage example of generating simulated leakage profiles in batches using multicores.

## Attacks in batches
* The argument `--exp` is provided to indicate the experiment number and can use `--exp=id`, with default eexperiment `id = 0`.

## Attacks running with file names
* The provided script simplifies the procedure for providing the input files, this is the same as running the following command.
* `python3 attack.py --manual <path to auxiliary dataset> <path to leakage file> <path to answer file> <path to report file>`
* aux_info (tuple frequencies), doc_matrix, the leakage profile, answer file for checking the correctness, and finally the output file report for recovery rate.

## Automatic log collection and leakage extraction
* The tasks for collecting logs / leakage extraction can be specified in the `run` function in `main.py`.

# Future use
MongoDB QE is currently in development. Planned future releases will include support for range, suffix, and prefix queries ([link](https://www.mongodb.com/docs/manual/core/queryable-encryption/#introduction)). We believe that our artifact can be extended to analyze the security of the these versitle functionalities, as well as the countermeasures against our attacks.

# FAQ for troubleshooting

1. What if the recommended version not found in the MongoDB release website?
For example, 6.0.7 has been updated to 6.0.8 version, then please edit the version number in the provided url found on the website.

2. Do I need root access to run the experiments?
No, please make sure you specify a local directory in `parameters.py` for your MongoDB default database.

3. Do I need to start `mongod` service when running E1?
No, you do not need to manually run `mongod` service, `python3 main.py` automatically handles that for you.

4. During installation, `libmongocrypt` is not found in the package source?
Use `sudo apt-get install -y libmongocrypt-dev` instead.

5. Running E1, `python3 main.py` throws the error `pymongo.errors.AutoReconnect: localhost:27017: connection closed"`?
   * This error may come up when the last execution of `main.py` is interrupted (e.g., manually or due to files are not found). Make sure you close `mongod` before running `main.py` again. For example, use `pkill mongod`.
   * Please make sure `python3 export_acs_data_sample.py` is run before `python3 main.py` to generate processed dataset and auxiliary information files.
6. If you encounter JSON decoding error for parsing 2012 aux_info when evaluating E3 or E4, this may due to the aux_info file is corrupted. Please re-download it from the repo.

7. Please find a suitable machine to run our evaluation. It is reported that Windows + Ubuntu VM on VirtualBox might not work for MongoDB V6.X.

# Acknowledgement
We are grateful to the anonymous reviewers for their contributions in the artifact evaluation process. By incorporating their suggestions and refining the artifact, we have significantly enhanced its quality!

# ChangeLog 
## 31/10/22 
We have made the following modifications to the code.
  
* Unifying the correctness check for both simulated and real leakage. Separate correctness checks from `parse_query_log`, `parse_op_log` were provided before and now a single `correctness.py` handles the correctness checking. Please see Appendix B of our paper for more details.
  
* `parse_query_log.py`: optimizing the memory usage by removing obsolete data structures, obsolete functions and merging `genFieldEquality` functionality into `genDocMatrix` (renamed from `genSnapshotInfo`). Adding `postProcessing` to deal with potential edge cases. Adding missing term "i" when exporting query leakage with additional length leakage for consistency.

* other files: removing obsolete parameters or printing statements.
  
The above changes only affect the correctness checks of `export_dataset_simulated.py` and `parse_op_log.py`.

We also provide some reference stats because some tasks are memory intensive. For example, to extract leakage with 8 cores on a full-scale database (ACS 2013) of 3 million records: for compaction leakage, `parse_op_log`, taking as input a 29 GB oplog, consumed about 7.7 GB memory, and completed within 10 minutes; for query leakage, `parse_query_log`, taking as input a 497 GB query_log, consumed roughly 47.5 GB memory, and took 4.8 hours to complete (3.6 hours for reading and parsing the log file and 1.2 hours for computing the leakage).